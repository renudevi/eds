<?php

class Model_TwentyFour extends Zend_Db_Table_Abstract {

    protected $_name = 'twentyfour';
    protected $_primary = 'twentyfour_id';
    protected $dbAdapter;

    public function init() {
        $this->dbAdapter = Zend_Registry::get('db');
    }

    /**
     * Creates new twentyfour
     * @param type $userId
     * @param type $twentyfourTitle
     * @param type $twentyfourText
     * @return type
     */
    public function createNewSuggestion($userId, $twentyfourTitle, $twentyfourText) {
        try {
            $row = $this->createRow();
            $row->twentyfour_employee_id = $userId;
            $row->twentyfour_title = $twentyfourTitle;
            $row->twentyfour_description = $twentyfourText;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getNewSuggestion() {
        try {
            $sql = "SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id                   
                    GROUP by twentyfour_id
                    HAVING suv > 0 OR sdv > 0
                    ORDER by diff desc,twentyfour_id desc";
            $stmt = $this->dbAdapter->query($sql);
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserSuggestionDetails($twentyfourId, $votingEmployeeId) {
        try {
            $twentyfourId = (int) $twentyfourId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $sql = "SELECT s.twentyfour_id as id,ev.up_vote as uv, ev.down_vote as dv, s.badge as badge
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = ev.voted_employee_id
                    WHERE s.twentyfour_id=? AND ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($twentyfourId, $votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserSuggDetailsByEmployeeId($votingEmployeeId) {

        try {
            $votingEmployeeId = (int) $votingEmployeeId;

            $sql = "SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function viewDisplayNewSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id   
                    WHERE badge = 'new'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function updateTotalVotesforSuggestionId($twentyfourId) {
        try {
            $twentyfourId = (int) $twentyfourId;
            $data = array(
                'total_votes' => new Zend_Db_Expr('total_votes + 1')
            );
            $where['twentyfour_id = ?'] = $twentyfourId;
            $n = $this->dbAdapter->update('twentyfour', $data, $where);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostrecentSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id 
                    WHERE badge = 'new'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostVotedSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    s.badge as badge, s.total_votes as totalvotes, sum(ev.up_vote) - sum(ev.down_vote) as diff
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id 
                    WHERE badge = 'new'
                    GROUP by twentyfour_id)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.badge,u.useruv,u.userdv,s.totalvotes,s.diff
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function resolvedSuggestions($votingEmployeeId) {
        try {

            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id   
                    WHERE badge = 'resolved'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function workinprogressSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id   
                    WHERE badge = 'workinprogress'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function archiveSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id   
                    WHERE badge = 'archive'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function addAdminComments($adminId, $admincomment, $adminbadge, $twentyfourId) {
        try {
            $adminId = (int) $adminId;
            $twentyfourId = (int) $twentyfourId;
            $data = array(
                'admin_comment' => $admincomment,
                'badge' => $adminbadge,
                'admin_id' => $adminId
            );

            $where['twentyfour_id = ?'] = $twentyfourId;
            $n = $this->dbAdapter->update('twentyfour', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostRecentSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,  s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge, s.admin_comment as admincomment
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id 
                    WHERE badge = 'new'
                    GROUP by twentyfour_id
                    ORDER by diff desc,twentyfour_id desc)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostVotedSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercachetwentyfour(SELECT s.twentyfour_id as usersuggid,
                    ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usertwentyfours
            $twentyfoursSql = "CREATE TEMPORARY TABLE suggcachetwentyfour(SELECT e.employee_email as email,s.twentyfour_title as title, s.twentyfour_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.twentyfour_id as id,
                    s.badge as badge, s.total_votes as totalvotes,s.admin_comment as admincomment, sum(ev.up_vote) - sum(ev.down_vote) as diff 
                    FROM employee_vote_twentyfour ev
                    INNER JOIN twentyfour s
                    USING (twentyfour_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.twentyfour_employee_id 
                    WHERE badge = 'new'
                    GROUP by twentyfour_id)";
            $twentyfoursStmt = $this->dbAdapter->query($twentyfoursSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,
                s.badge,u.useruv,u.userdv,s.totalvotes,s.admincomment,s.diff
                FROM suggcachetwentyfour as s
                LEFT JOIN usercachetwentyfour as u
                ON s.id = u.usersuggid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $twentyfoursDeleteSql = "drop table suggcachetwentyfour";
            $twentyfoursDeleteStmt = $this->dbAdapter->query($twentyfoursDeleteSql);

            $userSuggDeleteSql = "drop table usercachetwentyfour";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
