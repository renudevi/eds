<?php

class SearchController extends Zend_Controller_Action {

    protected $filterHtmlEntities;
    protected $zendLog;
    protected $solrModel;

    public function init() {
        //set default layout
        $this->_helper->layout->setLayout('default');
        $this->view->pageTitle = 'EDS Portal | Search';
        $this->view->js = array("handlebars.js", "magnific.js", "jPages.js");

        //check if session exists
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }
        //get user details fo the view
        $edsNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $edsNamespace->employeeName;
        $this->employeeId = $edsNamespace->employeeId;
        $this->view->username = $this->userName;


        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
        $this->zendLog = Zend_Registry::get('Zend_Log');
        $this->solrModel = new Model_SolrPHP();
       
    }

    public function indexAction() {
        try {           
            //get search Params
            $searchParam = trim($this->_getParam('q'));
            $page = trim($this->_getParam('page'));
            if(isset($searchParam) && !empty($searchParam)){
               $searchParamFiltered = $this->filterHtmlEntities->filter($searchParam);                
               $filterPageNumber = $this->filterHtmlEntities->filter($page);               
               $searchResults = $this->solrModel->getSuggestionsbyTitleandText($searchParamFiltered,$filterPageNumber);              
               $this->view->searchResults = $searchResults;
               $this->view->searchTerm = $searchParam;
               $this->view->resultCount = $searchResults->getNumFound();
            }else{
                return;
            }
                        
        } catch (Exception $ex) {
            echo $ex->getMessage();
            exit;
            $this->zendLog->log("Search Controller" . $ex->getMessage(), Zend_Log::ERROR);
        }
    }

}
