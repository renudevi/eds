<?php

class TwentyFourController extends Zend_Controller_Action {

    protected $employeeId = null;
    protected $userName = null;
    protected $filterHtmlEntities = null;
    protected $zendLog = null;

    public function preDispatch() {
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->assign('heading', 'Twenty Four 2014');
        Zend_Layout::getMvcInstance()->assign('saying', 'Showcase your Ideas, Prototypes & Hacks or vote for your favorite.');
    }

    public function init() {
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }

        //set default layout
        $this->_helper->layout->setLayout('default');
        $this->view->pageTitle = 'EDS Portal | Twenty Four';
        $this->view->css = array("carousel.css");
        $this->view->js = array("handlebars.js", "magnific.js", "twentyfour.js", "jPages.js");
        $edsNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $edsNamespace->employeeName;
        $this->employeeId = $edsNamespace->employeeId;
        $this->view->username = $this->userName;
        $this->twentyfourModel = new Model_TwentyFour();
        $this->employeeTwentyfourVoteModel = new Model_TwentyFourvote();
        $this->zendlog = Zend_Registry::get('Zend_Log');
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
    }

    public function indexAction() {
        // action body
    }

    public function ideasAction() {
        try {
            $this->_helper->layout->setLayout('defaultpbj');
            $viewSuggestions = $this->twentyfourModel->viewDisplayNewSuggestions($this->employeeId);
            $this->view->newSuggestions = $viewSuggestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage();
            $this->zendlog->log("PbjController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function createnewAction() {       
        $errorMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        /* $suggestTitle = $this->filterHtmlEntities->filter($this->getRequest()->getPost('suggtitle', NULL));
          $suggestText = $this->filterHtmlEntities->filter($this->getRequest()->getPost('suggdesc', NULL)); */
        $suggestTitle = $this->getRequest()->getPost('suggtitle', NULL);
        $suggestText = $this->getRequest()->getPost('suggdesc', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 10));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        
        if (!$noemptyValidator->isValid($suggestTitle)) {
            $errorMsg[] = "Suggestion Title cannot be empty";
        } else if (!$strlengthValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Title length should atleast 10chars ";
        }
          

        if (!$noemptyValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Title cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Text length should be atleast 10chars";
        }
       
       
        if (!count($errorMsg)) {
            $newSuggestionId = $this->twentyfourModel->createNewSuggestion($this->employeeId, $suggestTitle, $suggestText);
            $this->starterDummySuggestionVoteAction($newSuggestionId);
            if ($newSuggestionId) {
                $result = array(
                    "status" => "success",
                    "newid" => $newSuggestionId
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }
     

        echo json_encode($result);
    }

    private function getNewSuggestionAction() {
        try {
            $newSuggestions = $this->twentyfourModel->getNewSuggestion();
            return $newSuggestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage();
            $this->zendlog->log("PbjController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    private function starterDummySuggestionVoteAction($suggestionId) {
        $this->employeeTwentyfourVoteModel->createDummySuggestionVote($suggestionId);
    }

    public function upvoteAction() {        
        try {

            $errorMsg = array();
            $successUserMsg = array();
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
         

            //check session exists
            if (!Zend_Session::sessionExists()) {
                $errorMsg[] = "Please login to vote";
            }
        
            $suggestionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));            
            $usersession = new Zend_Session_Namespace('edsportal');
            $votingEmployeeId = $usersession->employeeId;           
        
            /* $suggestionId = 6;
              $votingEmployeeId = 1; */

            //check badge &  cast vote if user already not voted
            $suggResults = $this->twentyfourModel->getUserSuggestionDetails($suggestionId, $votingEmployeeId);
            // var_dump($suggResults);
            if (count($suggResults[0])) {

                $successUserMsg[] = 'Vote change cast done';
                if ($suggResults[0]['badge'] != "new") {
                    $errorMsg[] = 'Sorry voting is closed';
                } else if ($suggResults[0]['uv'] == '1') {
                    $errorMsg[] = 'Vote already casted';
                } else if ($suggResults[0]['dv'] == '1') {
                    $updatedVote = $this->employeeTwentyfourVoteModel->reverseCastDownVote($suggestionId, $votingEmployeeId);
                    if ($updatedVote) {
                        $successUserMsg[] = 'Vote change cast done';
                    }
                }
            } else {
                //user did not vote 
                $lastInsertVoteId = $this->employeeTwentyfourVoteModel->castUpVote($suggestionId, $votingEmployeeId);
                if ($lastInsertVoteId) {
                    $successUserMsg[] = 'Vote cast done';
                }
                //update totalvotes in suggestion table
                $this->twentyfourModel->updateTotalVotesforSuggestionId($suggestionId);
            }

            //send the current up/down votes for the  suggestion
            $suggesVotes = $this->employeeTwentyfourVoteModel->getCurrentVoteStatusBySuggestionId($suggestionId);

            if (count($errorMsg)) {
                $result = array(
                    "status" => "error",
                    "errormsg" => $errorMsg
                );
            } else {
                $result = array(
                    "status" => "success",
                    "successmsg" => $successUserMsg,
                    "votes" => $suggesVotes[0]
                );
            }

            echo json_encode($result);
        } catch (Zend_Exception $ex) {
           //throw new Zend_Db_Exception($ex->getMessage());
            $this->zendlog->log('TwentyFourController' . $ex->getMessage(), Zend_Log::ERROR);
        }
    }

    public function downvoteAction() {
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $suggestionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;
        /* $suggestionId = 4;
          $votingEmployeeId = 1; */

        //check badge & change up/down votes if user already votes
        $suggResults = $this->twentyfourModel->getUserSuggestionDetails($suggestionId, $votingEmployeeId);
        if (count($suggResults[0])) {
            if ($suggResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($suggResults[0]['dv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($suggResults[0]['uv'] == '1') {
                $updatedVote = $this->employeeTwentyfourVoteModel->reverseCastUpVote($suggestionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId = $this->employeeTwentyfourVoteModel->castDownVote($suggestionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in suggestion table
            $this->twentyfourModel->updateTotalVotesforSuggestionId($suggestionId);
        }

        //send the current up/down votes for the  suggestion
        $suggesVotes = $this->employeeTwentyfourVoteModel->getCurrentVoteStatusBySuggestionId($suggestionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $suggesVotes[0]
            );
        }

        echo json_encode($result);
    }

    public function mostrecentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->mostrecentSuggestions($votingEmployeeId);
        /* echo '<pre>';
          print_r($mostrecentSuggestions);
          echo '</pre>';
          return; */
        //echo json_encode($mostrecentSuggestions);
        $newsuggResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {

            foreach ($lookup as $key => $value) {

                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newsuggResults[] = $results;
        }


        echo json_encode($newsuggResults);
    }

    public function mostvotedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->mostVotedSuggestions($votingEmployeeId);
        $newsuggResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newsuggResults[] = $results;
        }
        echo json_encode($newsuggResults);
    }

    public function resolvedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->resolvedSuggestions($votingEmployeeId);
        $newsuggResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newsuggResults[] = $results;
        }
        echo json_encode($newsuggResults);
    }

    public function workinprogressAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->workinprogressSuggestions($votingEmployeeId);
        $workinprogressResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $workinprogressResults[] = $results;
        }
        echo json_encode($workinprogressResults);
    }

    public function newtabAction() {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $newsuggTabResults = $this->twentyfourModel->viewDisplayNewSuggestions($this->employeeId);
            $newsuggResults = array();
            $results = array();
            foreach ($newsuggTabResults as $lookup) {

                foreach ($lookup as $key => $value) {

                    if ($key == 'id') {
                        $results['upbtnid'] = 'up' . $value;
                        $results['downbtnid'] = 'down' . $value;
                        $results['upvoteid'] = 'upvote' . $value;
                        $results['downvoteid'] = 'downvote' . $value;
                        $results['votecolorid'] = 'votecolor' . $value;
                    }

                    if ($key == 'useruv') {
                        if (!is_null($value) && $value != '0') {
                            $results['upbtndimclass'] = 'arrowdim';
                        } else {
                            $results['upbtndimclass'] = '';
                        }
                    }

                    if ($key == 'userdv') {
                        if (!is_null($value) && $value != '0') {
                            $results['downbtndimclass'] = 'arrowdim';
                        } else {
                            $results['downbtndimclass'] = '';
                        }
                    }


                    if ($key == 'suv') {
                        $results['upvotecount'] = $value;
                        $suv = $value;
                    }

                    if ($key == 'sdv') {
                        $results['downvotecount'] = $value;
                        $sdv = $value;
                    }

                    $diff = $suv - $sdv;
                    if ($diff > 0) {
                        $results['votetextclass'] = 'upvotetext';
                    } else {
                        $results['votetextclass'] = 'downvotetext';
                    }

                    if ($key == "email") {
                        $results['suggemail'] = $value;
                    }

                    if ($key == 'title') {
                        $results['suggtitle'] = htmlspecialchars($value);
                    }

                    if ($key == 'text') {
                        $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                    }
                }
                $newsuggResults[] = $results;
            }
            echo json_encode($newsuggResults);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
