$(document).ready(function() {
    
    
    //Loading questions into the New tab.
    $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
        event.preventDefault();
        $("#questionsContainer").empty();

        var source = '{{#each .}}<div class="row eachquest eachsugg" id="{{this.questid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.questidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333;overflow-wrap: break-word">{{this.questtitle}}</div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap: break-word;">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminquestsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminquesterror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewquesterr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-pbj/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Answers</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s answers..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">You Asked Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each question end-->{{/each}}';



        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/newquesttab',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    console.log();
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000, 'swing');
                     //Navigation for Admin Mostvoted
                    $("div.holder").jPages({
                        containerID: "questionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });

                } else {
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });/* End New Tab*/
    
    


    //Loading questions into the RESOLVED tab.
    $('#pbjTab a[href="#archive"]').on('show.bs.tab', function(e) {
        // alert(e.target);
        $('#archivequestcontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="archiveinline-popups">' +
                '<a href="#archive-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#archive-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333;overflow-wrap:break-word">{{this.questtitle}} </div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" class="archivebadge">archive</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Answers&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/archive',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#archivequestcontainer').html(html).fadeIn(4000, 'swing');
                    //Navigation for Admin Archive Content
                        $("div.holder").jPages({
                            containerID: "archivequestcontainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                    //popup when clicking the up/down arrow 
                    $('.archiveinline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $('#archivequestcontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Archived Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });/* End Archive Tab*/




    //Loading questions into the RESOLVED tab.
    $('#pbjTab a[href="#resolved"]').on('show.bs.tab', function(e) {
        // alert(e.target);
        $('#resolvedquestioncontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="inline-popups">' +
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333;overflow-wrap:break-word">{{this.questtitle}} </div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word ">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" class="resolvedbadge">resolved</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Answers&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/resolvedquest',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#resolvedquestioncontainer').html(html).fadeIn(4000, 'swing');
                       //Navigation for Admin Archive Content
                        $("div.holder").jPages({
                            containerID: "resolvedquestioncontainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                    //popup when clicking the up/down arrow 
                    $('.inline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $('#resolvedquestioncontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Resolved Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });/* end resolvd tab*/

    //Loading questions into the WORKINPROGRESS tab.
    $('#pbjTab a[href="#workinprogress"]').on('show.bs.tab', function(e) {
        // e.target // activated tab
        // e.relatedTarget // previous tab
        // alert(e.target);       
        $('#workinprogressquestcontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="workininline-popups">' +
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333;overflow-wrap:break-word">{{this.questtitle}} </div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word ">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" class="workinprogressbadge">work-in-progress</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify">Admin&#39;s Answers&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/workinprogress',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#workinprogressquestcontainer').html(html).fadeIn(4000, 'swing');
                    //Navigation for Admin Archive Content
                    $("div.holder").jPages({
                        containerID: "workinprogressquestcontainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.workininline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $('#workinprogressquestcontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Work-in-progress Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });




    });


    //Admin show form to edit. 
    $('body').on('click', '.adminedit', function(event) {
        event.preventDefault();
        var editButtonId = this.id;
        $("#" + editButtonId).hide();
        var questionId = editButtonId.replace("edit", '');
        var questBox = "questid" + questionId;
        $("#" + questBox).addClass('admincommenthighlight');
        var commentFormId = "admincommentid" + questionId;
        console.log(commentFormId);
        $("#" + commentFormId).removeClass('hide');
        $("#questidborder" + questionId).removeClass('admincommborder');

    });

    //Hide adminform when cancel
    $('body').on('click', '.adminformcancel', function(event) {
        event.preventDefault();
        var canclebtnId = this.id;
        var questionId = canclebtnId.replace("adminformcancelid", '');
        var questBox = "questid" + questionId;
        $("#" + questBox).removeClass('admincommenthighlight');
        var commentFormId = "admincommentid" + questionId;
        $("#questidborder" + questionId).addClass('admincommborder');
        $("#" + commentFormId).addClass('hide');
        $('#admincommenttextid' + questionId).val('');
        $("#adminbadgeselectid" + questionId).val('none');
        $("#edit" + questionId).show();
        //remove any errors
        $("#adminquesterror" + questionId).addClass("hide");
    });

    // $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
    //create new questions form validation & form submit
    $('body').on('click', '.admincommentformsubmit', function(event) {

        event.preventDefault();
        var submitbtn = this.id;
        var questionId = submitbtn.replace("adminformsubmitid", "");
        var admincommenttext = $('#admincommenttextid' + questionId).val();
        var adminbadgeselect = $("#adminbadgeselectid" + questionId).val();

        var formData = {
            'questionid': questionId,
            'admincomment': admincommenttext,
            'adminbadge': adminbadgeselect
        };


        if (admincommenttext.length && adminbadgeselect !== "none") {
            $.ajax({
                type: 'POST',
                url: '/admin-you-asked/admincomment',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);                    
                    if (result.status === "success") {
                        $("#adminquesterror" + questionId).addClass("hide");
                        $("#adminquestsuccess" + questionId).removeClass("hide");
                        $("#adminquestsuccess" + questionId).fadeOut(1200);
                        $('#admincommenttextid' + questionId).val('');
                        $("#adminbadgeselectid" + questionId).val('none');
                        $("#questid" + questionId).fadeOut(2000);

                    } else if (result.status === "error") {
                        $("#adminquesterror" + questionId).removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#adminnewquesterr" + questionId).html(errorMsg);

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#adminquesterror" + questionId).removeClass("hide");
            $("#adminnewquesterr" + questionId).text('Admin Answer or badge cannot be empty');
        }

    });

    //  });




    $('body').on('click', '.voteupbtn', function(event) {
        event.preventDefault();
        var questionId = this.id;
        questionId = questionId.replace("up", '');
        var formData = {
            'id': questionId
        };
        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/upvote',
            data: formData,
            dataType: 'json',
            encode: true,
            cache: false,
            success: function(result) {
                //var result = JSON.parse(result);
                console.log(result);
                if (result.status === "success") {
                    $("#up" + questionId).addClass("arrowdim");
                    $("#down" + questionId).removeClass("arrowdim");
                    $("#upvote" + questionId).text(result.votes.suv);
                    $("#downvote" + questionId).text(result.votes.sdv);
                    if (result.votes.diff > 0) {
                        $("#votecolor" + questionId).addClass("upvotetext");
                        $("#votecolor" + questionId).removeClass("downvotetext");
                    } else {
                        $("#votecolor" + questionId).addClass("downvotetext");
                        $("#votecolor" + questionId).removeClass("upvotetext");
                    }

                } else if (result.status === "error") {
                    console.log(result.status);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });




    $('body').on('click', '.votedownbtn', function(event) {
        event.preventDefault();
        var questionId = this.id;
        questionId = questionId.replace("down", '');
        var formData = {
            'id': questionId
        };
        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/downvote',
            data: formData,
            dataType: 'json',
            encode: true,
            cache: false,
            success: function(result) {
                //var result = JSON.parse(result);
                console.log(result);
                if (result.status === "success") {
                    console.log(result.result);
                    $("#down" + questionId).addClass("arrowdim");
                    $("#up" + questionId).removeClass("arrowdim");
                    $("#upvote" + questionId).text(result.votes.suv);
                    $("#downvote" + questionId).text(result.votes.sdv);
                    if (result.votes.diff > 0) {
                        $("#votecolor" + questionId).addClass("upvotetext");
                        $("#votecolor" + questionId).removeClass("downvotetext");
                    } else {
                        $("#votecolor" + questionId).addClass("downvotetext");
                        $("#votecolor" + questionId).removeClass("upvotetext");
                    }


                } else if (result.status === "error") {
                    console.log(result.status);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });


    //show most recent question i.e questions just entered for admin panel
    $('#mostrecentquestbtn').click(function(event) {
        event.preventDefault();
        $("#questionsContainer").empty();

        var source = ' {{#each .}}<div class="row eachquest eachsugg" id="{{this.questid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.questidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.questtitle}}</div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminquestsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminquesterror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewquesterr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-pbj/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Answers</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s answers..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">You Asked Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each question end-->{{/each}}';


        var template = Handlebars.compile(source);
        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/adminmostrecent',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000, 'swing');
                        //Navigation for Admin mostrecent
                        $("div.holder").jPages({
                            containerID: "questionsContainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                } else {
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });



    //show most recent question i.e questions just entered
    $('#mostvotedquestbtn').click(function(event) {
        
        event.preventDefault();
        $("#questionsContainer").empty();

        var source = ' {{#each .}}<div class="row eachquest eachsugg" id="{{this.questid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.questidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="questestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.questtitle}}</div>' +
                '<div class="questestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.questtext}}}</div>' +
                '<div class="questestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminquestsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminquesterror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewquesterr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-pbj/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Answers</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s answers..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">You Asked Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each question end-->{{/each}}';



        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-you-asked/adminmostvoted',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000, 'swing');
                     //Navigation for Admin Mostvoted
                    $("div.holder").jPages({
                        containerID: "questionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });

                } else {
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Questions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });


    // make the first tab show after loading data
    $('#pbjTab a[href="#new"]').tab('show');


//create new questions form validation & form submit
    $('#questsubmit').click(function(event) {
        event.preventDefault();
        $("#newquestsuccess").addClass("hide");
        $("#questerror").addClass("hide");
        var questtitle = $('#questesttitle').val();
        var questtext = $('#newquestestdesc').val();
        var formData = {
            'questtitle': questtitle,
            'questdesc': questtext
        };
        if (questtitle.length && questtext.length) {
            $.ajax({
                type: 'POST',
                url: '/admin-you-asked/createnew',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);
                    //console.log(result);
                    if (result.status === "success") {
                        $("#newquestsuccess").removeClass("hide");
                        $("#newquestsuccess").fadeOut(1200);
                        $('#questesttitle').val('');
                        $('#newquestestdesc').val('');

                    } else if (result.status === "error") {
                        $("#questerror").removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#newquesterr").html(errorMsg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#questerror").removeClass("hide");
            $("#newquesterr").text('Question Title or Question description cannot be empty');
        }

    });
    
    //Navigation for Admin 
    $("div.holder").jPages({
        containerID: "questionsContainer",
        perPage: 10,
        startPage: 1,
        startRange: 2,
        midRange: 5,
        endRange: 1,
        first: "first",
        previous: "previous",
        next: "next",
        last: "last",
        animation :"fadeIn"
    });




    
    
    
    

});