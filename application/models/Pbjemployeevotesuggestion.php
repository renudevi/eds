<?php

class Model_Pbjemployeevotesuggestion extends Zend_Db_Table_Abstract {

    protected $_name = 'employee_vote_suggestion';
    protected $_primary = 'vote_suggestion_id';  
    protected $dbAdapter;
    
   
    public function init(){
        $this->dbAdapter = Zend_Registry::get('db');
    }

    public function createDummySuggestionVote($suggestionId) {
        try{
            $row = $this->createRow();
            $row->suggestion_id = (int) $suggestionId;
            $row->voted_employee_id = 1;
            $row->up_vote = 0;
            $row->down_vote = 0;
            return $row->save();            
        }catch(Zend_Db_Exception $ex){           
          throw new Zend_Db_Exception($ex->getMessage());            
        }         
    }

    public function castUpVote($suggestionId, $votingEmployeeId) {
        try{
            $row = $this->createRow();
            $row->suggestion_id = (int) $suggestionId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 1;
            $row->down_vote = 0;
            return $row->save();
        }catch(Zend_Db_Exception $ex){
            throw new Zend_Db_Exception($ex->getMessage()); 
        }
        
    }

    public function castDownVote($suggestionId, $votingEmployeeId) {
        try {
            $row = $this->createRow();
            $row->suggestion_id = (int) $suggestionId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 0;
            $row->down_vote = 1;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getCurrentVoteStatusBySuggestionId($suggestionId) {
        try {
            $suggestionId = (int) $suggestionId;
            $sql = "SELECT sum(up_vote) as suv, sum(down_vote) as sdv, sum(up_vote)- sum(down_vote) as diff
                FROM employee_vote_suggestion
                WHERE suggestion_id = ?";
            $stmt = $this->dbAdapter->query($sql, array($suggestionId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastUpVote($suggestionId, $votingEmployeeId) {
        try {
            $suggestionId = (int) $suggestionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 1,
                'up_vote' => 0
            );

            $where['suggestion_id = ?'] = $suggestionId;
            $where['voted_employee_id = ?'] = $votingEmployeeId;
            $n = $this->dbAdapter->update('employee_vote_suggestion', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastDownVote($suggestionId, $votingEmployeeId) {
        try {
            $suggestionId = (int) $suggestionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 0,
                'up_vote' => 1
            );
            $where['voted_employee_id=?'] = $votingEmployeeId;
            $where['suggestion_id=?'] = $suggestionId;
            $n = $this->dbAdapter->update('employee_vote_suggestion', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
