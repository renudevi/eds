<?php

class AdminYouAskedController extends Zend_Controller_Action {

    protected $employeeId = null;
    protected $userName = null;
    protected $filterHtmlEntities = null;
    protected $questionModel = null;
    protected $employeeQuestionVoteModel = null;
    protected $zendlog = null;

    public function preDispatch() {
        try {
            $acccesHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('acl');
            $resource = 'pbjAdminArea';
            if (!$acccesHelper->checkAccessRights($resource)) {
                $this->_helper->redirector('index', 'index');
                return;
            }
            parent::preDispatch();
            Zend_Layout::getMvcInstance()->assign('heading', 'Ask a Question');
            Zend_Layout::getMvcInstance()->assign('saying', 'Find your answers here');
        } catch (Zend_Exception $ex) {
            $this->zendlog->log("AdminYourquestionsController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function init() {
        //check if admin and session exists
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }

        //set default layout
        $this->_helper->layout->setLayout('adminpbj');
        $this->view->pageTitle = 'EDS Portal | YourQuestions Admin';
        $this->view->js = array("handlebars.js", "magnific.js", "adminyouasked.js", "jPages.js");
        $esdNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $esdNamespace->employeeName;
        $this->employeeId = $esdNamespace->employeeId;
        $this->view->username = $this->userName;
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
        $this->questionModel = new Model_Yourquestion();
        $this->employeeQuestionVoteModel = new Model_Yourquestionemployeevote();
        $this->zendlog = Zend_Registry::get('Zend_Log');
    }

    public function indexAction() {
        try {
            $viewQuestions = $this->questionModel->viewDisplayNewQuestions($this->employeeId);
            $this->view->newQuestions = $viewQuestions;
        } catch (Zend_Exception $ex) {
            $this->zendlog->log("AdminYourquestionsController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function createnewAction() {
        $errorMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        /* $questestTitle = $this->filterHtmlEntities->filter($this->getRequest()->getPost('questtitle', NULL));
          $questestText = $this->filterHtmlEntities->filter($this->getRequest()->getPost('questdesc', NULL)); */
        $questestTitle = $this->getRequest()->getPost('questtitle', NULL);
        $questestText = $this->getRequest()->getPost('questdesc', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 10));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        if (!$noemptyValidator->isValid($questestTitle)) {
            $errorMsg[] = "Question Title cannot be empty";
        } else if (!$strlengthValidator->isValid($questestText)) {
            $errorMsg[] = "Question Title length should atleast 10chars ";
        }

        if (!$noemptyValidator->isValid($questestText)) {
            $errorMsg[] = "Question Title cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($questestText)) {
            $errorMsg[] = "Question Text length should be atleast 10chars";
        }

        if (!count($errorMsg)) {
            $newQuestionId = $this->questionModel->createNewQuestion($this->employeeId, $questestTitle, $questestText);
            $this->starterDummyQuestionVote($newQuestionId);
            if ($newQuestionId) {
                $result = array(
                    "status" => "success",
                    "newid" => $newQuestionId
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }

        echo json_encode($result);
    }

    private function getNewQuestion() {
        try {
            $newQuestions = $this->questionModel->getNewQuestion();
            return $newQuestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage()
            $this->zendlog->log("AdminYourquestionsController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    private function starterDummyQuestionVote($questionId) {
        $this->employeeQuestionVoteModel->createDummyQuestionVote($questionId);
    }

    public function upvoteAction() {
        try{
            
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $questionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal');
        $votingEmployeeId = $usersession->employeeId;
        /* $questionId = 6;
          $votingEmployeeId = 1; */

        //check badge &  cast vote if user already not voted
        $questResults = $this->questionModel->getUserQuestionDetails($questionId, $votingEmployeeId);
        // var_dump($questResults);
        if (count($questResults[0])> 0) {

            $successUserMsg[] = 'Vote change cast done';
            if ($questResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($questResults[0]['uv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($questResults[0]['dv'] == '1') {
                $updatedVote = $this->employeeQuestionVoteModel->reverseCastDownVote($questionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId = $this->employeeQuestionVoteModel->castUpVote($questionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in question table
            $this->questionModel->updateTotalVotesforQuestionId($questionId);
        }

        //send the current up/down votes for the  question
        $questesVotes = $this->employeeQuestionVoteModel->getCurrentVoteStatusByQuestionId($questionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $questesVotes[0]
            );
        }

        echo json_encode($result);
            
        }catch(Zend_Exception $ex){
            echo $ex->getMessage();
        }
    }

    public function downvoteAction() {
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $questionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;
        /* $questionId = 4;
          $votingEmployeeId = 1; */

        //check badge & change up/down votes if user already votes
        $questResults = $this->questionModel->getUserQuestionDetails($questionId, $votingEmployeeId);
        if (count($questResults[0])) {
            if ($questResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($questResults[0]['dv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($questResults[0]['uv'] == '1') {
                $updatedVote = $this->employeeQuestionVoteModel->reverseCastUpVote($questionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId = $this->employeeQuestionVoteModel->castDownVote($questionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in question table
            $this->questionModel->updateTotalVotesforQuestionId($questionId);
        }

        //send the current up/down votes for the  question
        $questesVotes = $this->employeeQuestionVoteModel->getCurrentVoteStatusByQuestionId($questionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $questesVotes[0]
            );
        }

        echo json_encode($result);
    }

    public function resolvedquestAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $resolvedQuestions = $this->questionModel->resolvedQuestions($votingEmployeeId);
        $resolvedResults = array();
        $results = array();
        foreach ($resolvedQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $resolvedResults[] = $results;
        }
        echo json_encode($resolvedResults);
    }

    public function workinprogressAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $workinprogressQuestions = $this->questionModel->workinprogressQuestions($votingEmployeeId);
        $workinprogressResults = array();
        $results = array();
        foreach ($workinprogressQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $workinprogressResults[] = $results;
        }
        echo json_encode($workinprogressResults);
    }

    public function archiveAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $archiveQuestions = $this->questionModel->archiveQuestions($votingEmployeeId);
        $archiveResults = array();
        $results = array();
        foreach ($archiveQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $archiveResults[] = $results;
        }
        echo json_encode($archiveResults);
    }

    /**
     * function for submitting the admin comments
     */
    public function admincommentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $questionId = $this->getRequest()->getPost('questionid', NULL);
        $admincomment = $this->getRequest()->getPost('admincomment', NULL);
        $adminbadge = $this->getRequest()->getPost('adminbadge', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 4));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        $intValidator = new Zend_Validate_Int();

        if (!$intValidator->isValid($questionId)) {
            $errorMsg[] = "Invalid Question Id";
        }

        if (!$noemptyValidator->isValid($adminbadge)) {
            $errorMsg[] = "Question Badge cannot be empty";
        } else if (!$strlengthValidator->isValid($adminbadge) || $adminbadge == "none") {
            $errorMsg[] = "Invalid Question Badge";
        }

        if (!$noemptyValidator->isValid($admincomment)) {
            $errorMsg[] = "Admin answer cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($admincomment)) {
            $errorMsg[] = "Admin answer length should be atleast 30chars";
        }

        if (!count($errorMsg)) {
            //$adminId, $admincomment, $adminbadge, $questionId
            $createnewAdminQuestt = $this->questionModel->addAdminComments($this->employeeId, $admincomment, $adminbadge, $questionId);
            if ($createnewAdminQuestt) {
                $result = array(
                    "status" => "success"
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }

        echo json_encode($result);
    }

    public function adminmostrecentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->adminMostRecentQuestions($votingEmployeeId);
        /* echo '<pre>';
          print_r($mostrecentQuestions);
          echo '</pre>';
          return;
          //echo json_encode($mostrecentQuestions); */
        $newquestResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {

            foreach ($lookup as $key => $value) {

                if ($key == 'id') {
                    //admin template id
                    $results['questid'] = 'questid' . $value;
                    $results['questidborder'] = 'questidborder' . $value;

                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;

                    //AdminId values for form

                    $results['edit'] = 'edit' . $value;
                    $results['admincommentid'] = 'admincommentid' . $value;
                    $results['admincommentformid'] = 'admincommentformid' . $value;
                    $results['admincommenttextid'] = 'admincommenttextid' . $value;
                    $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                    $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                    $results['adminformcancelid'] = 'adminformcancelid' . $value;
                    $results['adminquestsuccess'] = 'adminquestsuccess' . $value;
                    $results['adminquesterror'] = 'adminquesterror' . $value;
                    $results['adminnewquesterr'] = 'adminnewquesterr' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newquestResults[] = $results;
        }
        echo json_encode($newquestResults);
    }

    public function adminmostvotedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->adminMostVotedQuestions($votingEmployeeId);
        $newquestResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    //admin template id
                    $results['questid'] = 'questid' . $value;
                    $results['questidborder'] = 'questidborder' . $value;

                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;

                    //AdminId values for form

                    $results['edit'] = 'edit' . $value;
                    $results['admincommentid'] = 'admincommentid' . $value;
                    $results['admincommentformid'] = 'admincommentformid' . $value;
                    $results['admincommenttextid'] = 'admincommenttextid' . $value;
                    $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                    $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                    $results['adminformcancelid'] = 'adminformcancelid' . $value;
                    $results['adminquestsuccess'] = 'adminquestsuccess' . $value;
                    $results['adminquesterror'] = 'adminquesterror' . $value;
                    $results['adminnewquesterr'] = 'adminnewquesterr' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newquestResults[] = $results;
        }
        echo json_encode($newquestResults);
    }

    public function newquesttabAction() {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $newquestTabResults = $this->questionModel->viewDisplayNewQuestions($this->employeeId);
            $newquestResults = array();
            $results = array();
            foreach ($newquestTabResults as $lookup) {
                foreach ($lookup as $key => $value) {
                    if ($key == 'id') {
                        //admin template id
                        $results['questid'] = 'questid' . $value;
                        $results['questidborder'] = 'questidborder' . $value;

                        $results['upbtnid'] = 'up' . $value;
                        $results['downbtnid'] = 'down' . $value;
                        $results['upvoteid'] = 'upvote' . $value;
                        $results['downvoteid'] = 'downvote' . $value;
                        $results['votecolorid'] = 'votecolor' . $value;

                        //AdminId values for form
                        $results['edit'] = 'edit' . $value;
                        $results['admincommentid'] = 'admincommentid' . $value;
                        $results['admincommentformid'] = 'admincommentformid' . $value;
                        $results['admincommenttextid'] = 'admincommenttextid' . $value;
                        $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                        $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                        $results['adminformcancelid'] = 'adminformcancelid' . $value;
                        $results['adminquestsuccess'] = 'adminquestsuccess' . $value;
                        $results['adminquesterror'] = 'adminquesterror' . $value;
                        $results['adminnewquesterr'] = 'adminnewquesterr' . $value;
                    }

                    if ($key == 'useruv') {
                        if (!is_null($value) && $value != '0') {
                            $results['upbtndimclass'] = 'arrowdim';
                        } else {
                            $results['upbtndimclass'] = '';
                        }
                    }

                    if ($key == 'userdv') {
                        if (!is_null($value) && $value != '0') {
                            $results['downbtndimclass'] = 'arrowdim';
                        } else {
                            $results['downbtndimclass'] = '';
                        }
                    }


                    if ($key == 'suv') {
                        $results['upvotecount'] = $value;
                        $suv = $value;
                    }

                    if ($key == 'sdv') {
                        $results['downvotecount'] = $value;
                        $sdv = $value;
                    }

                    $diff = $suv - $sdv;
                    if ($diff > 0) {
                        $results['votetextclass'] = 'upvotetext';
                    } else {
                        $results['votetextclass'] = 'downvotetext';
                    }

                    if ($key == "email") {
                        $results['questemail'] = $value;
                    }

                    if ($key == 'title') {
                        $results['questtitle'] = htmlspecialchars($value);
                    }

                    if ($key == 'text') {
                        $results['questtext'] = nl2br(htmlspecialchars($value), true);
                    }
                }
                $newquestResults[] = $results;
            }
            echo json_encode($newquestResults);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
