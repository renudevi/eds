<?php

/**
 * @see Zend_Controller_Action_Helper_Abstract
 */
require_once 'Zend/Controller/Action/Helper/Abstract.php';

/**
 * Description of Acl
 *
 * @author botre001
 */
class Zend_Controller_Action_Helper_Acl extends Zend_Controller_Action_Helper_Abstract {

    protected $acl;
    protected $role;

    protected function getAcl() {
        if (is_null($this->acl)) {
            
            $this->acl = new Zend_Acl();
            //add roles
            $this->acl->addRole(new Zend_Acl_Role('user'));
            $this->acl->addRole(new Zend_Acl_Role('pbjadmin'),'user');
            $this->acl->addRole(new Zend_Acl_Role('dqadmin'),'user');
            $this->acl->addRole(new Zend_Acl_Role('twentyfouradmin'),'user');
            
            //add resources
            $this->acl->addResource(new Zend_Acl_Resource('userArea'));
            $this->acl->addResource(new Zend_Acl_Resource('pbjAdminArea'),'userArea');
            $this->acl->addResource(new Zend_Acl_Resource('dqAdminArea'),'userArea');
            $this->acl->addResource(new Zend_Acl_Resource('TwentyFourAdminArea'),'userArea');
            
            $this->acl->allow('user','userArea');
            $this->acl->allow('pbjadmin','userArea');
            $this->acl->allow('pbjadmin', 'pbjAdminArea');
            $this->acl->allow('dqadmin','dqAdminArea');
            $this->acl->allow('twentyfouradmin','TwentyFourAdminArea');
            
            
            $this->acl->deny('user','pbjAdminArea');
            $this->acl->deny('user','dqAdminArea');
            $this->acl->deny('pbjadmin','dqAdminArea');
            $this->acl->deny('dqadmin','pbjAdminArea');
            $this->acl->deny('user','TwentyFourAdminArea');
            $this->acl->deny('pbjadmin','TwentyFourAdminArea');
            $this->acl->deny('dqadmin','TwentyFourAdminArea');
        }
        return $this->acl;
    }
    
    protected function getRole(){
        if(is_null($this->role)){
            $sessionNamespace = new Zend_Session_Namespace('edsportal');
            $userrole = isset($sessionNamespace->userType)? $sessionNamespace->userType : 'user';
            $this->role = $userrole;
        }
        return $this->role;
        
    }
    
    public function checkAccessRights($userResource, $userRole = NULL, $userPrivilege = NULL){
        
        $acl = $this->getAcl();
        $role = $this->getRole();
        $allowed =  $acl->isAllowed($role, $userResource, $privilege);
        return $allowed;
        
    }

}
