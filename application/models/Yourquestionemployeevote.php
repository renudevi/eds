<?php

class Model_Yourquestionemployeevote extends Zend_Db_Table_Abstract {

    protected $_name = 'employee_vote_question';
    protected $_primary = 'vote_question_id';
    protected $dbAdapter;

    public function init() {
        $this->dbAdapter = Zend_Registry::get('db');
    }

    public function createDummyQuestionVote($questionId) {
        try {
            $row = $this->createRow();
            $row->question_id = (int) $questionId;
            $row->voted_employee_id = 1;
            $row->up_vote = 0;
            $row->down_vote = 0;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function castUpVote($questionId, $votingEmployeeId) {
        try {
            $row = $this->createRow();
            $row->question_id = (int) $questionId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 1;
            $row->down_vote = 0;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function castDownVote($questionId, $votingEmployeeId) {
        try {
            $row = $this->createRow();
            $row->question_id = (int) $questionId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 0;
            $row->down_vote = 1;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getCurrentVoteStatusByQuestionId($questionId) {
        try {
            $questionId = (int) $questionId;
            $sql = "SELECT sum(up_vote) as suv, sum(down_vote) as sdv, sum(up_vote)- sum(down_vote) as diff
                FROM employee_vote_question
                WHERE question_id = ?";
            $stmt = $this->dbAdapter->query($sql, array($questionId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastUpVote($questionId, $votingEmployeeId) {
        try {
            $questionId = (int) $questionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 1,
                'up_vote' => 0
            );

            $where['question_id = ?'] = $questionId;
            $where['voted_employee_id = ?'] = $votingEmployeeId;
            $n = $this->dbAdapter->update('employee_vote_question', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastDownVote($questionId, $votingEmployeeId) {
        try {
            $questionId = (int) $questionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 0,
                'up_vote' => 1
            );
            $where['voted_employee_id=?'] = $votingEmployeeId;
            $where['question_id=?'] = $questionId;
            $n = $this->dbAdapter->update('employee_vote_question', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
