$(document).ready(function() {
    
    //Loading suggestions into the New tab.
    $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
          event.preventDefault();
        $("#suggestionsContainer").empty();
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{{this.suggtitle}}}</div>'+
            '<div class="suggestexplain"  style="font-size:90%;margin-top:5px;">{{{this.suggtext}}}</div>'+
            '<div class="suggestuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>'+
            '</div>'+
        '</div>'+
    '</div>'+
'</div>{{/each}}';

      
        var template = Handlebars.compile(source);
         
         $.ajax({
                type: 'POST',
                url: '/pbj/newsuggtab',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  //console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000,'swing');
                     //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                      
                 }else{
                    $('#suggestionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

    });/* End New Tab*/
    
    
    
    //Loading suggestions into the RESOLVED tab.
    $('#pbjTab a[href="#resolved"]').on('show.bs.tab', function(e) {
       // e.target // activated tab
       // e.relatedTarget // previous tab
       // console.log(e.target);       
       $('#resolvedsuggestioncontainer').empty();
       var source ='{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">'+
        '<div class="col-xs-12" style="min-height:120px;padding:10px;">'+
            '<div class="col-xs-1" style="padding-left:30px"><div class="inline-popups">'+
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                    '<span class="glyphicon glyphicon-chevron-up" ></span>'+
                    '</button></a>'+
                '<br/>'+
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                    '<span class="glyphicon glyphicon-chevron-down"></span>'+
                    '</button></a>'+
                '</div></div>'+
            '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
                '</div>'+

            '<div class="col-xs-10" style="min-height:80px">'+
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}} </div>'+
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>'+
                '<div class="suggestuser" style="float:right;margin-top:20px;">'+
                    '<a href="#" class="resolvedbadge">resolved</a>'+
                    '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>'+
                    '</div>'+
                '</div>'+

            '</div>'+
        '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">'+
            '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Comments&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>'+
            '</div>'+
        '</div>{{/each}}';
        var template = Handlebars.compile(source);
        
         $.ajax({
                type: 'POST',
                url: '/pbj/resolvedsugg',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#resolvedsuggestioncontainer').html(html).fadeIn(4000,'swing');  
                      //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "resolvedsuggestioncontainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.inline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500, 
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true 
                    });
                 }else{
                    $('#resolvedsuggestioncontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Resolved Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        
    });/* end resolvd tab*/
    
       //Loading suggestions into the WORKINPROGRESS tab.
    $('#pbjTab a[href="#workinprogress"]').on('show.bs.tab', function(e) {
       // e.target // activated tab
       // e.relatedTarget // previous tab
       // console.log(e.target);       
       $('#workinprogresssuggcontainer').empty();
       var source ='{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">'+
        '<div class="col-xs-12" style="min-height:120px;padding:10px;">'+
            '<div class="col-xs-1" style="padding-left:30px"><div class="workininline-popups">'+
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                    '<span class="glyphicon glyphicon-chevron-up" ></span>'+
                    '</button></a>'+
                '<br/>'+
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                    '<span class="glyphicon glyphicon-chevron-down"></span>'+
                    '</button></a>'+
                '</div></div>'+
            '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
                '</div>'+
            '<div class="col-xs-10" style="min-height:80px">'+
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}} </div>'+
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>'+
                '<div class="suggestuser" style="float:right;margin-top:20px;">'+
                    '<a href="#" class="workinprogressbadge">work-in-progress</a>'+
                    '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>'+
                    '</div>'+
                '</div>'+

            '</div>'+
        '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">'+
            '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Comments&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>'+
            '</div>'+
        '</div>{{/each}}';
        var template = Handlebars.compile(source);
        
         $.ajax({
                type: 'POST',
                url: '/pbj/workinprogress',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#workinprogresssuggcontainer').html(html).fadeIn(4000,'swing');  
                    //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "workinprogresssuggcontainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.workininline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500, 
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true 
                    });
                 }else{
                    $('#workinprogresssuggcontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
            
         
    
        
    });
    
   
    
    $('body').on('click','.voteupbtn',function(event){        
        event.preventDefault();
        var suggestionId = this.id;
        suggestionId = suggestionId.replace("up",'');
        var formData = {
            'id':suggestionId 
        };
        $.ajax({
                type: 'POST',
                url: '/pbj/upvote',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    //var result = JSON.parse(result);
                     console.log(result);
                    if (result.status === "success") {                        
                        $("#up"+suggestionId).addClass("arrowdim");
                        $("#down"+suggestionId).removeClass("arrowdim");
                        $("#upvote"+suggestionId).text(result.votes.suv);
                        $("#downvote"+suggestionId).text(result.votes.sdv); 
                         if(result.votes.diff > 0){
                          $("#votecolor"+suggestionId).addClass("upvotetext"); 
                          $("#votecolor"+suggestionId).removeClass("downvotetext"); 
                        }else{
                          $("#votecolor"+suggestionId).addClass("downvotetext"); 
                          $("#votecolor"+suggestionId).removeClass("upvotetext"); 
                        }

                    } else if (result.status === "error") {
                       console.log(result.status);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

    });
    
    
    
    
       $('body').on('click','.votedownbtn',function(event){        
        event.preventDefault();
        var suggestionId = this.id;
        suggestionId = suggestionId.replace("down",'');
        var formData = {
            'id':suggestionId 
        };
        $.ajax({
                type: 'POST',
                url: '/pbj/downvote',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    //var result = JSON.parse(result);
                    console.log(result);
                    if (result.status === "success") {
                        console.log(result.result);
                        $("#down"+suggestionId).addClass("arrowdim");
                        $("#up"+suggestionId).removeClass("arrowdim");
                        $("#upvote"+suggestionId).text(result.votes.suv);
                        $("#downvote"+suggestionId).text(result.votes.sdv);
                        if(result.votes.diff > 0){
                          $("#votecolor"+suggestionId).addClass("upvotetext"); 
                          $("#votecolor"+suggestionId).removeClass("downvotetext"); 
                        }else{
                          $("#votecolor"+suggestionId).addClass("downvotetext"); 
                          $("#votecolor"+suggestionId).removeClass("upvotetext"); 
                        }
                        

                    } else if (result.status === "error") {
                       console.log(result.status);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

    });
    
    
        //show most recent suggestion i.e suggestions just entered
    $('#mostrecentsuggbtn').click(function(event) {
        event.preventDefault();
        $("#suggestionsContainer").empty();
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}}</div>'+
            '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>'+
            '<div class="suggestuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>'+
            '</div>'+
        '</div>'+

    '</div>'+
'</div>{{/each}}';

        var template = Handlebars.compile(source);
         $.ajax({
                type: 'POST',
                url: '/pbj/mostrecent',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000,'swing');
                    //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                 }else{
                    $('#suggestionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
      
    });
    
    
    
     //show most recent suggestion i.e suggestions just entered
    $('#mostvotedsuggbtn').click(function(event) {
        event.preventDefault();
        $("#suggestionsContainer").empty();
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{{this.suggtitle}}}</div>'+
            '<div class="suggestexplain"  style="font-size:90%;margin-top:5px;">{{{this.suggtext}}}</div>'+
            '<div class="suggestuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>'+
            '</div>'+
        '</div>'+
    '</div>'+
'</div>{{/each}}';

      
        var template = Handlebars.compile(source);
         
         $.ajax({
                type: 'POST',
                url: '/pbj/mostvoted',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  //console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000,'swing');
                     //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                      
                 }else{
                    $('#suggestionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
      
    });
    
 
    
    
    
    
    // make the first tab show after loading data
    $('#pbjTab a[href="#new"]').tab('show') ;

  

//create new suggestions form validation & form submit
    $('form').submit(function(event) {
        event.preventDefault();
        $("#suggsuccess").addClass("hide");
        $("#suggerror").addClass("hide");
        var suggtitle = $('#suggesttitle').val();
        var suggtext = $('#suggestdesc').val();
        var formData = {
            'suggtitle': suggtitle,
            'suggdesc': suggtext
        };
        if (suggtitle.length && suggtext.length) {
            $.ajax({
                type: 'POST',
                url: '/pbj/createnew',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);
                    console.log(result);
                    if (result.status === "success") {
                        $("#suggsuccess").removeClass("hide");
                        $("#suggsuccess").fadeOut(1200);
                        $('#suggesttitle').val('');
                        $('#suggestdesc').val('');

                    } else if (result.status === "error") {
                        $("#suggerror").removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#newsuggerr").html(errorMsg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#suggerror").removeClass("hide");
            $("#newsuggerr").text('Suggestion Title or Suggestion Text cannot be empty');
        }

    });
    
        //Navigation for Suggestions Content
        $("div.holder").jPages({
            containerID: "suggestionsContainer",
           perPage: 10,
            startPage: 1,
            startRange: 2,
            midRange: 5,
            endRange: 1,
            first: "first",
            previous: "previous",
            next: "next",
            last: "last",
            animation :"fadeIn"
        });

});