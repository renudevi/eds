<?php

class AccountController extends Zend_Controller_Action {
    
    protected $ldap;
    protected $filterHtmlEntities;
    protected $employeeModel;
    protected $zendlog;

    public function init() {
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
        $this->ldap = "";
        $this->employeeModel = new Model_Employee();
        $this->zendlog = Zend_Registry::get('Zend_Log');
    }

    public function indexAction() {
        // action body
    }

    public function loginAction() {
        if (Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
            return;
        }
        $this->view->errormsg;
        $validationMessages = array();

        if ($this->getRequest()->isPost()) {
            $validationMessages = array();
            $useracctname = $this->getRequest()->getPost('edsusername', NULL);
            $userpassword = $this->getRequest()->getPost('edspassword', NULL);
            $useracctname = $this->filterHtmlEntities->filter($useracctname);
            $alphanumValidator = new Zend_Validate_Alnum();
            $notemptyValidator = new Zend_Validate_NotEmpty();
            //$passwordValidator = new Zend_Validate_Regex(array('pattern' => '//'));

            if (!$notemptyValidator->isValid($useracctname)) {
                array_push($validationMessages, 'Please enter Username');                
            }else if ($notemptyValidator->isValid($useracctname) && !$alphanumValidator->isValid($useracctname)) {
                    array_push($validationMessages, 'Please enter a valid Username');                   
            }
            if (!$notemptyValidator->isValid($userpassword)) {
                array_push($validationMessages, 'Please enter Password');               
            }/*else if ($notemptyValidator->isValid($userpassword) && !$passwordValidator->isValid($useracctname)) {
                    array_push($validationMessages, 'Please enter a valid Password');
            }*/

            if (count($validationMessages)) {
                //echo json_encode($validationMessages);
                $this->view->errormsg = $validationMessages;
                return;
            }


            $options = array(
                'host' => 'ped-new.staples.com:1389',
                'useStartTls' => false,
                'useSsl' => false,
                'username' => 'uid=adcost_svc,ou=applications,dc=staples,dc=com',
                'password' => 'F2@DTbuK1',
                'accountDomainName' => 'staples.com',
                'accountCanonicalForm' => 2,
                'accountFilterFormat' => '(&(objectClass=person)(uid=%s))',
                'baseDn' => 'dc=staples,dc=com',
                'bindRequiresDn' => true
            );

            try {
                $this->ldap = new Zend_Ldap($options);
                $this->ldap->bind($useracctname, $userpassword);
                $acctname = $this->ldap->getCanonicalAccountName($useracctname);              
                $seatchtrem = "uid=".$acctname;
                $employeesearch = $this->ldap->searchEntries('(&(objectClass=person)('. $seatchtrem.'))'); 
                $employeeId = (int)$employeesearch[0]["employeenumber"][0];
                $employeeEmail = $employeesearch[0]["mail"][0]; 
                $employeeName = $employeesearch[0]["cn"][0];
                if ($acctname == strtolower($useracctname)) {
                    $employeeDetails = array();
                    //Start user session
                    Zend_Session::start();
                    $usersession = new Zend_Session_Namespace('edsportal');                   
                    $usersession->loggedin = TRUE;
                    $userResults = $this->checkAdmin($employeeId);                    
                    $usersession->isadmin = $userResults['isadmin'];
                    //$usersession->isadmin = FALSE;
                    $usersession->userType = $userResults['admintype'];
                    ($usersession->isadmin)? $employeeDetails['isadmin'] = 'yes': $employeeDetails['isadmin']='no';                   
                    $usersession->uid = $employeeDetails['uid'] = $useracctname;
                    $usersession->employeeId = $employeeDetails['employeeid']= $employeeId;
                    $usersession->employeeEmail = $employeeDetails['email'] = $employeeEmail;
                    $usersession->employeeName = $employeeDetails['name'] = $employeeName;
                    
                    
                    if($this->employeeModel->checkEmployeeExists($employeeId)){
                        $this->employeeModel->updateLastLoggedin($employeeId);
                    }else{
                        $this->employeeModel->addEmployeetoEDS($employeeDetails); 
                    } 
                    if($usersession->isadmin){
                      $this->_helper->redirector('index', 'admin-pbj');
                      return;  
                    }else{
                       $this->_helper->redirector('index', 'index');
                      return;  
                    }                    
                    
                } else {
                    array_push($validationMessages, 'Authentication Failed');
                }
            } catch (Zend_Exception $ex) {
               // echo $ex->getMessage(); 
                $this->zendlog->log("AccountController".$ex->getMessage() , Zend_Log::INFO);
            }
        }
    }

    public function logoutAction() {
        try {
            if (Zend_Session::sessionExists()) {
                Zend_Session::destroy($remove_cookie = true, $readonly = true);
                Zend_Session::namespaceUnset('edsportal');
                $this->ldap->disconnect();
            }
            $this->_helper->redirector('login', 'account');
            return;
        } catch (Zend_Exception $ex) {
            $this->_helper->redirector('login', 'account');
            return;
        }
    }
    
    private function checkAdmin($uid) {

        $results = array();
        //add people as admin
        $pbjadmin = array(1712591, 1699000);
        $dqadmin = array(1712591, 1699000, 1226773);

        if (in_array($uid, $pbjadmin)) {
            $results['isadmin'] = TRUE;
            $results['admintype'] = 'pbjadmin';
            return $results;
        } else if (in_array($uid, $dqadmin)) {
            $results['isadmin'] = TRUE;
            $results['admintype'] = 'dqadmin';
            return $results;
        }else {
            $results['isadmin'] = FALSE;
            $results['admintype'] = 'user';
            return $results;
        }
    }

}
?>


