<?php

class Model_TwentyFourvote extends Zend_Db_Table_Abstract
{    

    protected $_name = 'employee_vote_twentyfour';
    protected $_primary = 'vote_twentyfour_id';  
    protected $dbAdapter;
    
   
    public function init(){
        $this->dbAdapter = Zend_Registry::get('db');
    }

    public function createDummySuggestionVote($twentyfourId) {
        try{
            $row = $this->createRow();
            $row->twentyfour_id = (int) $twentyfourId;
            $row->voted_employee_id = 1;
            $row->up_vote = 0;
            $row->down_vote = 0;
            return $row->save();            
        }catch(Zend_Db_Exception $ex){           
          throw new Zend_Db_Exception($ex->getMessage());            
        }         
    }

    public function castUpVote($twentyfourId, $votingEmployeeId) {
        try{
            $row = $this->createRow();
            $row->twentyfour_id = (int) $twentyfourId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 1;
            $row->down_vote = 0;
            return $row->save();
        }catch(Zend_Db_Exception $ex){
            throw new Zend_Db_Exception($ex->getMessage()); 
        }
        
    }

    public function castDownVote($twentyfourId, $votingEmployeeId) {
        try {
            $row = $this->createRow();
            $row->twentyfour_id = (int) $twentyfourId;
            $row->voted_employee_id = (int) $votingEmployeeId;
            $row->up_vote = 0;
            $row->down_vote = 1;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getCurrentVoteStatusBySuggestionId($twentyfourId) {
        try {
            $twentyfourId = (int) $twentyfourId;
            $sql = "SELECT sum(up_vote) as suv, sum(down_vote) as sdv, sum(up_vote)- sum(down_vote) as diff
                FROM employee_vote_twentyfour
                WHERE twentyfour_id = ?";
            $stmt = $this->dbAdapter->query($sql, array($twentyfourId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastUpVote($twentyfourId, $votingEmployeeId) {
        try {
            $twentyfourId = (int) $twentyfourId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 1,
                'up_vote' => 0
            );

            $where['twentyfour_id = ?'] = $twentyfourId;
            $where['voted_employee_id = ?'] = $votingEmployeeId;
            $n = $this->dbAdapter->update('employee_vote_twentyfour', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function reverseCastDownVote($twentyfourId, $votingEmployeeId) {
        try {
            $twentyfourId = (int) $twentyfourId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $data = array(
                'down_vote' => 0,
                'up_vote' => 1
            );
            $where['voted_employee_id=?'] = $votingEmployeeId;
            $where['twentyfour_id=?'] = $twentyfourId;
            $n = $this->dbAdapter->update('employee_vote_twentyfour', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }


}

