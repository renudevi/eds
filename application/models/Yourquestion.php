<?php

class Model_Yourquestion extends Zend_Db_Table_Abstract {

    protected $_name = 'question';
    protected $_primary = 'question_id';
    protected $dbAdapter;

    public function init() {
        $this->dbAdapter = Zend_Registry::get('db');
    }

    /**
     * Creates new question
     * @param type $userId
     * @param type $questionTitle
     * @param type $questionText
     * @return type
     */
    public function createNewQuestion($userId, $questionTitle, $questionText) {
        try {
            $row = $this->createRow();
            $row->question_employee_id = $userId;
            $row->question_title = $questionTitle;
            $row->question_description = $questionText;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getNewQuestion() {
        try {
            $sql = "SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id                   
                    GROUP by question_id
                    HAVING suv > 0 OR sdv > 0
                    ORDER by diff desc,question_id desc";
            $stmt = $this->dbAdapter->query($sql);
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserQuestionDetails($questionId, $votingEmployeeId) {
        try {
            $questionId = (int) $questionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $sql = "SELECT s.question_id as id,ev.up_vote as uv, ev.down_vote as dv, s.badge as badge
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = ev.voted_employee_id
                    WHERE s.question_id=? AND ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($questionId, $votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserQuestDetailsByEmployeeId($votingEmployeeId) {

        try {
            $votingEmployeeId = (int) $votingEmployeeId;

            $sql = "SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function viewDisplayNewQuestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id   
                    WHERE badge = 'new'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function updateTotalVotesforQuestionId($questionId) {
        try {
            $questionId = (int) $questionId;
            $data = array(
                'total_votes' => new Zend_Db_Expr('total_votes + 1')
            );
            $where['question_id = ?'] = $questionId;
            $n = $this->dbAdapter->update('question', $data, $where);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostrecentQuestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id 
                    WHERE badge = 'new'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostVotedQuestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    s.badge as badge, s.total_votes as totalvotes, sum(ev.up_vote) - sum(ev.down_vote) as diff
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id 
                    WHERE badge = 'new'
                    GROUP by question_id)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.badge,u.useruv,u.userdv,s.totalvotes,s.diff
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function resolvedQuestions($votingEmployeeId) {
        try {

            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id   
                    WHERE badge = 'resolved'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function workinprogressQuestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id   
                    WHERE badge = 'workinprogress'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function archiveQuestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id   
                    WHERE badge = 'archive'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function addAdminComments($adminId, $admincomment, $adminbadge, $questionId) {
        try {
            $adminId = (int) $adminId;
            $questionId = (int) $questionId;
            $data = array(
                'admin_comment' => $admincomment,
                'badge' => $adminbadge,
                'admin_id' => $adminId
            );

            $where['question_id = ?'] = $questionId;
            $n = $this->dbAdapter->update('question', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostRecentQuestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,  s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge, s.admin_comment as admincomment
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id 
                    WHERE badge = 'new'
                    GROUP by question_id
                    ORDER by diff desc,question_id desc)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostVotedQuestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userQuestSql = "CREATE TEMPORARY TABLE usercachequestion(SELECT s.question_id as userquestid,
                    ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    WHERE ev.voted_employee_id=?)";
            $userQuestStmt = $this->dbAdapter->query($userQuestSql, array($votingEmployeeId));
            //$result = $userQuestStmt->fetchAll();
            //getting all userquestions
            $questionsSql = "CREATE TEMPORARY TABLE questcache(SELECT e.employee_email as email,s.question_title as title, s.question_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.question_id as id,
                    s.badge as badge, s.total_votes as totalvotes,s.admin_comment as admincomment, sum(ev.up_vote) - sum(ev.down_vote) as diff 
                    FROM employee_vote_question ev
                    INNER JOIN question s
                    USING (question_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.question_employee_id 
                    WHERE badge = 'new'
                    GROUP by question_id)";
            $questionsStmt = $this->dbAdapter->query($questionsSql);
            //$questResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,
                s.badge,u.useruv,u.userdv,s.totalvotes,s.admincomment,s.diff
                FROM questcache as s
                LEFT JOIN usercachequestion as u
                ON s.id = u.userquestid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $questionsDeleteSql = "drop table questcache";
            $questionsDeleteStmt = $this->dbAdapter->query($questionsDeleteSql);

            $userQuestDeleteSql = "drop table usercachequestion";
            $userQuestDeleteStmt = $this->dbAdapter->query($userQuestDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
