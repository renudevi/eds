$(document).ready(function() {    
    if ($(".oopsdiv").length > 0) {
        $('#adminpbjnav').removeClass("show");
        $('#adminpbjnav').addClass("hide");
    } else {
        $('#adminpbjnav').removeClass("hide");
        $('#adminpbjnav').addClass("show");
    }
    
      /*In new tab List View or Grid View*/ 
   $("#listview").click(function(){
       console.log("clicked");
      $("#suggestionsContainer").empty();  
      var source = '{{#each .}}<div class="row">'+
                '<div class="col-xs-12 listvieweachdiv">' +
                '<div class="col-xs-1">' +
                '<div class="{{this.liststatusclass}}">&nbsp;</div>' +
                '</div>' +
                '<div class="col-xs-11">' +
                '<div class="listsuggestheading">{{this.listsuggheadingtext}}</div>' +
                '<div class="listsuggestuser">{{this.listsuggusettext}}</div>' +
                '</div>' +
                '</div>' +
                '</div>{{/each}}';
         var template = Handlebars.compile(source);
         
           $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/newsuggtablistview',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    //console.log();
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000, 'swing');
                     //Navigation for Admin Mostvoted
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });

                } else {
                    $('#adminpbjnav').addClass("hide");
                    $('#suggestionsContainer').html($('<div class="row oopsdiv"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });


              
   });
        
        
        
        

    
    //Loading suggestions into the New tab.
    $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
        event.preventDefault();
        $("#suggestionsContainer").empty();       

        var source = '{{#each .}}<div class="row eachsugg" id="{{this.suggid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.suggidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333;overflow-wrap:break-word !important;">{{this.suggtitle}}</div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word !important;">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminsuggsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminsuggerror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewsuggerr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-twenty-four/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Comments</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s comments..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">PB&J Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each suggestion end-->{{/each}}';



        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/newtab',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    //console.log();
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000, 'swing');
                     //Navigation for Admin Mostvoted
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });

                } else {
                    $('#adminpbjnav').addClass("hide");
                    $('#suggestionsContainer').html($('<div class="row oopsdiv"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
        
 
    });/* End New Tab*/
    
    


    //Loading suggestions into the RESOLVED tab.
    $('#pbjTab a[href="#archive"]').on('show.bs.tab', function(e) {
   
        // alert(e.target);
        $('#archivesuggcontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="archiveinline-popups">' +
                '<a href="#archive-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#archive-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}} </div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;overflow-wrap:break-word;">' +
                '<a href="#" class="archivebadge">archive</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Comments&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/archive',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
               // console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#archivesuggcontainer').html(html).fadeIn(4000, 'swing');
                    //Navigation for Admin Archive Content
                        $("div.holder").jPages({
                            containerID: "archivesuggcontainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                    //popup when clicking the up/down arrow 
                    $('.archiveinline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $('#archivetabnav').addClass("hide");
                    $('#archivesuggcontainer').html($('<div class="row oopsdiv"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Archived Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
        if ($(".oopsdiv").length > 0) {
            $('#archivetabnav').removeClass("show");
            $('#archivetabnav').addClass("hide");
        } else {
            $('#archivetabnav').removeClass("hide");
            $('#archivetabnav').addClass("show");
        }

    });/* End Archive Tab*/




    //Loading suggestions into the RESOLVED tab.
    $('#pbjTab a[href="#resolved"]').on('show.bs.tab', function(e) {
        // alert(e.target);
        $('#resolvedsuggestioncontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="inline-popups">' +
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}} </div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px;overflow-wrap:break-word;">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;overflow-wrap:break-word;">' +
                '<a href="#" class="resolvedbadge">resolved</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Comments&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/resolved',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#resolvedsuggestioncontainer').html(html).fadeIn(4000, 'swing');
                       //Navigation for Admin Archive Content
                        $("div.holder").jPages({
                            containerID: "resolvedsuggestioncontainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                    //popup when clicking the up/down arrow 
                    $('.inline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $("#resolvedtabnav").addClass("hide");
                    $('#resolvedsuggestioncontainer').html($('<div class="row oopsdiv"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Resolved Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
        
        if ($(".oopsdiv").length > 0) {
            $('#resolvedtabnav').removeClass("show");
            $('#resolvedtabnav').addClass("hide");
        } else {
            $('#resolvedtabnav').removeClass("hide");
            $('#resolvedtabnav').addClass("show");
        }

    });/* end resolvd tab*/

    //Loading suggestions into the WORKINPROGRESS tab.
    $('#pbjTab a[href="#workinprogress"]').on('show.bs.tab', function(e) {
        // e.target // activated tab
        // e.relatedTarget // previous tab
        // alert(e.target);       
        $('#workinprogresssuggcontainer').empty();
        var source = '{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">' +
                '<div class="col-xs-12" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px"><div class="workininline-popups">' +
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button></a>' +
                '<br/>' +
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button></a>' +
                '</div></div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-10" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}} </div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" class="workinprogressbadge">work-in-progress</a>' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">' +
                '<div><span style="font-weight: bold;font-size:14px;text-align:justify ">Admin&#39;s Comments&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>' +
                '</div>' +
                '</div>{{/each}}';
        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/workinprogress',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#workinprogresssuggcontainer').html(html).fadeIn(4000, 'swing');
                    //Navigation for Admin Archive Content
                    $("div.holder").jPages({
                        containerID: "workinprogresssuggcontainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.workininline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500,
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true
                    });
                } else {
                    $("#workinprogrestabsnav").addClass("hide");
                    $('#workinprogresssuggcontainer').html($('<div class="row oopsdiv"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

        if ($(".oopsdiv").length > 0) {
            $('#workinprogrestabsnav').removeClass("show");
            $('#workinprogrestabsnav').addClass("hide");
        } else {
            $('#workinprogrestabsnav').removeClass("hide");
            $('#workinprogrestabsnav').addClass("show");
        }


    });


    //Admin show form to edit. 
    $('body').on('click', '.adminedit', function(event) {
        event.preventDefault();
        var editButtonId = this.id;
        $("#" + editButtonId).hide();
        var suggestionId = editButtonId.replace("edit", '');
        var suggBox = "suggid" + suggestionId;
        $("#" + suggBox).addClass('admincommenthighlight');
        var commentFormId = "admincommentid" + suggestionId;
        console.log(commentFormId);
        $("#" + commentFormId).removeClass('hide');
        $("#suggidborder" + suggestionId).removeClass('admincommborder');

    });

    //Hide adminform when cancel
    $('body').on('click', '.adminformcancel', function(event) {
        event.preventDefault();
        var canclebtnId = this.id;
        var suggestionId = canclebtnId.replace("adminformcancelid", '');
        var suggBox = "suggid" + suggestionId;
        $("#" + suggBox).removeClass('admincommenthighlight');
        var commentFormId = "admincommentid" + suggestionId;
        $("#suggidborder" + suggestionId).addClass('admincommborder');
        $("#" + commentFormId).addClass('hide');
        $('#admincommenttextid' + suggestionId).val('');
        $("#adminbadgeselectid" + suggestionId).val('none');
        $("#edit" + suggestionId).show();
        //remove any errors
        $("#adminsuggerror" + suggestionId).addClass("hide");
    });

    // $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
    //create new suggestions form validation & form submit
    $('body').on('click', '.admincommentformsubmit', function(event) {

        event.preventDefault();
        var submitbtn = this.id;
        var suggestionId = submitbtn.replace("adminformsubmitid", "");
        var admincommenttext = $('#admincommenttextid' + suggestionId).val();
        var adminbadgeselect = $("#adminbadgeselectid" + suggestionId).val();

        var formData = {
            'suggestionid': suggestionId,
            'admincomment': admincommenttext,
            'adminbadge': adminbadgeselect
        };


        if (admincommenttext.length && adminbadgeselect !== "none") {
            $.ajax({
                type: 'POST',
                url: '/admin-twenty-four/admincomment',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);                    
                    if (result.status === "success") {
                        $("#adminsuggerror" + suggestionId).addClass("hide");
                        $("#adminsuggsuccess" + suggestionId).removeClass("hide");
                        $("#adminsuggsuccess" + suggestionId).fadeOut(1200);
                        $('#admincommenttextid' + suggestionId).val('');
                        $("#adminbadgeselectid" + suggestionId).val('none');
                        $("#suggid" + suggestionId).fadeOut(2000);

                    } else if (result.status === "error") {
                        $("#adminsuggerror" + suggestionId).removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#adminnewsuggerr" + suggestionId).html(errorMsg);

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#adminsuggerror" + suggestionId).removeClass("hide");
            $("#adminnewsuggerr" + suggestionId).text('Admin Comment or Admin badge cannot be empty');
        }

    });

    //  });




    $('body').on('click', '.voteupbtn', function(event) {
        event.preventDefault();
        var suggestionId = this.id;
        suggestionId = suggestionId.replace("up", '');
        var formData = {
            'id': suggestionId
        };
        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/upvote',
            data: formData,
            dataType: 'json',
            encode: true,
            cache: false,
            success: function(result) {
                //var result = JSON.parse(result);
                console.log(result);
                if (result.status === "success") {
                    $("#up" + suggestionId).addClass("arrowdim");
                    $("#down" + suggestionId).removeClass("arrowdim");
                    $("#upvote" + suggestionId).text(result.votes.suv);
                    $("#downvote" + suggestionId).text(result.votes.sdv);
                    if (result.votes.diff > 0) {
                        $("#votecolor" + suggestionId).addClass("upvotetext");
                        $("#votecolor" + suggestionId).removeClass("downvotetext");
                    } else {
                        $("#votecolor" + suggestionId).addClass("downvotetext");
                        $("#votecolor" + suggestionId).removeClass("upvotetext");
                    }

                } else if (result.status === "error") {
                    console.log(result.status);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });




    $('body').on('click', '.votedownbtn', function(event) {
        event.preventDefault();
        var suggestionId = this.id;
        suggestionId = suggestionId.replace("down", '');
        var formData = {
            'id': suggestionId
        };
        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/downvote',
            data: formData,
            dataType: 'json',
            encode: true,
            cache: false,
            success: function(result) {
                //var result = JSON.parse(result);
                console.log(result);
                if (result.status === "success") {
                    console.log(result.result);
                    $("#down" + suggestionId).addClass("arrowdim");
                    $("#up" + suggestionId).removeClass("arrowdim");
                    $("#upvote" + suggestionId).text(result.votes.suv);
                    $("#downvote" + suggestionId).text(result.votes.sdv);
                    if (result.votes.diff > 0) {
                        $("#votecolor" + suggestionId).addClass("upvotetext");
                        $("#votecolor" + suggestionId).removeClass("downvotetext");
                    } else {
                        $("#votecolor" + suggestionId).addClass("downvotetext");
                        $("#votecolor" + suggestionId).removeClass("upvotetext");
                    }


                } else if (result.status === "error") {
                    console.log(result.status);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });


    //show most recent suggestion i.e suggestions just entered for admin panel
    $('#mostrecentsuggbtn').click(function(event) {
        event.preventDefault();
        $("#suggestionsContainer").empty();

        var source = ' {{#each .}}<div class="row eachsugg" id="{{this.suggid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.suggidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}}</div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminsuggsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminsuggerror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewsuggerr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-twenty-four/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Comments</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s comments..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">PB&J Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each suggestion end-->{{/each}}';


        var template = Handlebars.compile(source);
        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/adminmostrecent',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000, 'swing');
                        //Navigation for Admin mostrecent
                        $("div.holder").jPages({
                            containerID: "suggestionsContainer",
                            perPage: 10,
                            startPage: 1,
                            startRange: 2,
                            midRange: 5,
                            endRange: 1,
                            first: "first",
                            previous: "previous",
                            next: "next",
                            last: "last",
                            animation :"fadeIn"
                        });
                } else {
                    $('#suggestionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });



    //show most recent suggestion i.e suggestions just entered
    $('#mostvotedsuggbtn').click(function(event) {
        
        event.preventDefault();
        $("#suggestionsContainer").empty();

        var source = ' {{#each .}}<div class="row eachsugg" id="{{this.suggid}}">' +
                '<div class="col-xs-12 admincommborder" id="{{this.suggidborder}}" style="min-height:120px;padding:10px;">' +
                '<div class="col-xs-1" style="padding-left:30px">' +
                '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px;">' +
                '<span class="glyphicon glyphicon-chevron-up" ></span>' +
                '</button>' +
                '<br/>' +
                '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">' +
                '<span class="glyphicon glyphicon-chevron-down"></span>' +
                '</button>' +
                '</div>' +
                '<div class="col-xs-1" style="margin-left:-20px;height:100px;">' +
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>' +
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>' +
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>' +
                '</div>' +
                '<div class="col-xs-8" style="min-height:80px">' +
                '<div class="suggestheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.suggtitle}}</div>' +
                '<div class="suggestexplain"  style="font-size:90%;margin-top:5px; ">{{{this.suggtext}}}</div>' +
                '<div class="suggestuser" style="float:right;margin-top:20px;">' +
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.suggemail}}</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-2" style="margin-left:20px;height:100px;margin-top:20px;">' +
                '<button type="button" id="{{this.edit}}" class="adminedit btn btn-default btn-sm">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>' +
                '</div>' +
                '</div>' +
                '<!--admin comment start form div--> <div class="col-xs-12 hide" id="{{this.admincommentid}}">' +
                '<div class="row hide" style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"  id="{{this.adminsuggsuccess}}">' +
                ' <div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-success">' +
                '<a href="#" class="alert-link" style="text-align:center">Your&#8242;s Comment is Submitted</a>' +
                '</div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<div class="row hide"   style="margin-right:100px; text-align:center;margin-top:10px;margin-bottom:-20px"   id="{{this.adminsuggerror}}">' +
                '<div class="col-xs-3"></div>' +
                '<div class="col-xs-6">' +
                '<div class="alert alert-dismissable alert-danger">' +
                '<a href="#" id="{{this.adminnewsuggerr}}" class="alert-link" style="text-align:center"></a>' +
                ' </div>' +
                '</div>' +
                '<div class="col-xs-3"></div>' +
                '</div>' +
                '<form class="form-horizontal" method="post" action="admin-twenty-four/comment"  id="{{this.admincommentformid}}" style="margin-top:50px;border-bottom: 1px dotted #ddd;">' +
                '<div class="form-group">' +
                '<label for="{{this.admincommenttextid}}" class="col-xs-2 control-label">Admin&#8242;s Comments</label>' +
                '<div class="col-xs-10">' +
                '<textarea class="form-control" rows="3" id="{{this.admincommenttextid}}" style="width: 700px; height: 200px;" placeholder="Admin&#8242;s comments..."></textarea>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="{{this.adminbadgeselectid}}" class="col-xs-2 control-label">PB&J Queue</label>' +
                '<div class="col-xs-3">' +
                '<select class="form-control" id="{{this.adminbadgeselectid}}">' +
                '<option value="none">Select one</option>' +
                '<option value="workinprogress">Work-in-progress</option>' +
                '<option value="resolved">Resolved</option>' +
                '<option value="archive">Archive</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<div class="col-xs-10 col-xs-offset-2">' +
                '<button type="submit" class="btn btn-primary admincommentformsubmit" id="{{this.adminformsubmitid}}"  style="margin-top:40px;">Submit</button>&nbsp;&nbsp;&nbsp;' +
                '<button type="submit" class="btn btn-default adminformcancel" id="{{this.adminformcancelid}}"  style="margin-top:40px;">Cancel</button>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '</div><!--each suggestion end-->{{/each}}';



        var template = Handlebars.compile(source);

        $.ajax({
            type: 'POST',
            url: '/admin-twenty-four/adminmostvoted',
            data: "",
            dataType: '',
            encode: true,
            cache: false,
            success: function(result) {
                //console.log(result);
                if (result.length > 2) {
                    var data = JSON.parse(result);
                    var html = template(data);
                    $('#suggestionsContainer').html(html).fadeIn(4000, 'swing');
                     //Navigation for Admin Mostvoted
                    $("div.holder").jPages({
                        containerID: "suggestionsContainer",
                        perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });

                } else {
                    $('#suggestionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Suggestions!!</h4></div></div><div class="col-xs-2"></div>'));
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });

    });


    // make the first tab show after loading data
    $('#pbjTab a[href="#new"]').tab('show');


//create new suggestions form validation & form submit
    $('#suggsubmit').click(function(event) {
        event.preventDefault();
        $("#newsuggsuccess").addClass("hide");
        $("#suggerror").addClass("hide");
        var suggtitle = $('#suggesttitle').val();
        var suggtext = $('#newsuggestdesc').val();
        var formData = {
            'suggtitle': suggtitle,
            'suggdesc': suggtext
        };
        if (suggtitle.length && suggtext.length) {
            $.ajax({
                type: 'POST',
                url: '/admin-twenty-four/createnew',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);
                    console.log(result);
                    if (result.status === "success") {
                        $("#newsuggsuccess").removeClass("hide");
                        $("#newsuggsuccess").fadeOut(1200);
                        $('#suggesttitle').val('');
                        $('#newsuggestdesc').val('');

                    } else if (result.status === "error") {
                        $("#suggerror").removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#newsuggerr").html(errorMsg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#suggerror").removeClass("hide");
            $("#newsuggerr").text('Suggestion Title or Suggestion Text cannot be empty');
        }

    });
    
    //Navigation for Admin 
    $("div.holder").jPages({
        containerID: "suggestionsContainer",
        perPage: 10,
        startPage: 1,
        startRange: 2,
        midRange: 5,
        endRange: 1,
        first: "first",
        previous: "previous",
        next: "next",
        last: "last",
        animation :"fadeIn"
    });




    
    
    
    

});