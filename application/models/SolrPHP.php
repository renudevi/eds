<?php

/**
 * Description of Model_Solr
 *
 * @author botre001
 */
class Model_SolrPHP {

    protected $solrClient;
    protected $solrConfig;
    protected $zendHttp;

    public function __construct() {
        if (get_parent_class($this) != false) {
            parent::__construct();
        }
        if (method_exists($this, 'init')) {
            $this->init();
        }
        $this->solrConfig = array(
            "endpoint" => array(
                "localhost" => array(
                    "host" => "127.0.0.1",
                    "port" => "8088",
                    "path" => "/solr",
                    "core" => "edsstaples")
        ));

        $this->solrClient = new Solarium\Client($this->solrConfig);
    }

    public function checkSolrHealth(){
        
    }
    
    public function getSuggestionsbyTitleandText($searchParam, $startPage){
       
        try{            
       // $this->solrClient->setAdapter('Solarium_Client_Adapter_ZendHttp');
       // $zendHttp = $this->solrClient->getAdapter()->getZendHttp();
       // $zendHttp->setAuth('renuka', 'Soldier8!', Zend_Http_Client::AUTH_BASIC);
          if(!isset($startPage)  || empty($startPage)){
                   $startPage = 1;
               }
           $query = $this->solrClient->createSelect();
           $searchdata ="suggtitle:$searchParam OR suggtext:$searchParam";
           $query->setStart($startPage)->setRows(15);
           $query->setQuery($searchdata);
           $resultSet = $this->solrClient->select($query);
           return $resultSet;
            
        } catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }        
    }

    public function addSuggtoSolr(Array $suggParams) {
        try {
            //get update query instance
            $update = $this->solrClient->createUpdate();
            $doc = $update->createDocument();
            $doc->suggid = $suggParams['suggId'];
            $doc->suggtitle = $suggParams['suggTitle'];
            $doc->suggtext = $suggParams['suggText'];
            $doc->suggemail = $suggParams['suggEmail'];
            $doc->suggbadge = $suggParams['suggBadge'];
            $update->addDocuments($doc);
            $update->addCommit();
            $result = $this->solrClient->update($update);
            return $result->getStatus();
        } catch (Exception $ex) {
           throw new Exception($ex->getMessage());
        }
    }

}
