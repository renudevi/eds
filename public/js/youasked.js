$(document).ready(function() {
    
    //Loading questions into the New tab.
    $('#pbjTab a[href="#new"]').on('show.bs.tab', function(e) {
          event.preventDefault();
        $("#questionsContainer").empty();
        $("#newquestpbjnav").addClass("hide");
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="questheading" style="font-size:120%;line-height: 1.3;color:#333">{{{this.questtitle}}}</div>'+
            '<div class="questexplain"  style="font-size:90%;margin-top:5px;">{{{this.questtext}}}</div>'+
            '<div class="questuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>'+
            '</div>'+
        '</div>'+
    '</div>'+
'</div>{{/each}}';

      
        var template = Handlebars.compile(source);
         
         $.ajax({
                type: 'POST',
                url: '/youasked/newquesttab',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  console.log(result);
                 if(result.length > 2){
                  $("#newquestpbjnav").addClass("show");
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000,'swing');
                     //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "questionsContainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                      
                 }else{
                    $("#newquestpbjnav").addClass("hide");
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Questions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

    });/* End New Tab*/
    
    
    
    //Loading questions into the RESOLVED tab.
    $('#pbjTab a[href="#resolved"]').on('show.bs.tab', function(e) {
       // e.target // activated tab
       // e.relatedTarget // previous tab
       // console.log(e.target);       
       $('#resolvedquestioncontainer').empty();
       var source ='{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">'+
        '<div class="col-xs-12" style="min-height:120px;padding:10px;">'+
            '<div class="col-xs-1" style="padding-left:30px"><div class="inline-popups">'+
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                    '<span class="glyphicon glyphicon-chevron-up" ></span>'+
                    '</button></a>'+
                '<br/>'+
                '<a href="#small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                    '<span class="glyphicon glyphicon-chevron-down"></span>'+
                    '</button></a>'+
                '</div></div>'+
            '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
                '</div>'+

            '<div class="col-xs-10" style="min-height:80px">'+
                '<div class="questheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.questtitle}} </div>'+
                '<div class="questexplain"  style="font-size:90%;margin-top:5px; ">{{{this.questtext}}}</div>'+
                '<div class="questuser" style="float:right;margin-top:20px;">'+
                    '<a href="#" class="resolvedbadge">resolved</a>'+
                    '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>'+
                    '</div>'+
                '</div>'+

            '</div>'+
        '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">'+
            '<div><span style="font-weight: bold;font-size:14px;text-align:justify "> Admin&#39;s Answers&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>'+
            '</div>'+
        '</div>{{/each}}';
        var template = Handlebars.compile(source);
        
         $.ajax({
                type: 'POST',
                url: '/youasked/resolvedquest',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                //console.log(result);
                 if(result.length > 2){
                  $("#resolvedpbjnav").addClass("show");
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#resolvedquestioncontainer').html(html).fadeIn(4000,'swing');  
                      //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "resolvedquestioncontainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.inline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500, 
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true 
                    });
                 }else{
                    $("#resolvedpbjnav").addClass("hide");
                    $('#resolvedquestioncontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Resolved Questions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        
    });/* end resolvd tab*/
    
       //Loading questions into the WORKINPROGRESS tab.
    $('#pbjTab a[href="#workinprogress"]').on('show.bs.tab', function(e) {
       // e.target // activated tab
       // e.relatedTarget // previous tab
       // console.log(e.target);      
       $("#workpbjnav").addClass("hide");
       $('#workinprogressquestcontainer').empty();
       var source ='{{#each .}}<div class="row eachsugg" style="background-color:#F9F9F9">'+
        '<div class="col-xs-12" style="min-height:120px;padding:10px;">'+
            '<div class="col-xs-1" style="padding-left:30px"><div class="workininline-popups">'+
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button  id="{{this.upbtnid}}" type="button" class="resolvevoteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                    '<span class="glyphicon glyphicon-chevron-up" ></span>'+
                    '</button></a>'+
                '<br/>'+
                '<a href="#workin-small-dialog" data-effect="popup-with-zoom-anim"><button id="{{this.downbtnid}}" type="button" class="resolvevotedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                    '<span class="glyphicon glyphicon-chevron-down"></span>'+
                    '</button></a>'+
                '</div></div>'+
            '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
                '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
                '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
                '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
                '</div>'+
            '<div class="col-xs-10" style="min-height:80px">'+
                '<div class="questheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.questtitle}} </div>'+
                '<div class="questexplain"  style="font-size:90%;margin-top:5px; ">{{{this.questtext}}}</div>'+
                '<div class="questuser" style="float:right;margin-top:20px;">'+
                    '<a href="#" class="workinprogressbadge">work-in-progress</a>'+
                    '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>'+
                    '</div>'+
                '</div>'+

            '</div>'+
        '<div class="col-xs-12" style="min-height:70px;padding:10px;border-bottom: 1px dotted #ddd;margin-top: 10px;">'+
            '<div><span style="font-weight: bold;font-size:14px;text-align:justify "> Admin&#39;s Answers&nbsp;&nbsp;-&nbsp;&nbsp;</span>{{this.admincomment}}</div>'+
            '</div>'+
        '</div>{{/each}}';
        var template = Handlebars.compile(source);
        
         $.ajax({
                type: 'POST',
                url: '/youasked/workinprogress',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  //console.log(result);                  
                 if(result.length > 2){
                   $("#workpbjnav").addClass("show");
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#workinprogressquestcontainer').html(html).fadeIn(4000,'swing');  
                    //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "workinprogressquestcontainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                    //popup when clicking the up/down arrow 
                    $('.workininline-popups').magnificPopup({
                        delegate: 'a',
                        removalDelay: 500, 
                        callbacks: {
                            beforeOpen: function() {
                                this.st.mainClass = this.st.el.attr('data-effect');
                            }
                        },
                        midClick: true 
                    });
                 }else{
                    $("#workpbjnav").addClass("hide");
                    $('#workinprogressquestcontainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Work-in-progress Questions!!</h4></div></div><div class="col-xs-2"></div>'));  
                    
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });   
        
    });
    
   
    
    $('body').on('click','.voteupbtn',function(event){        
        event.preventDefault();
        var questionId = this.id;
        questionId = questionId.replace("up",'');
        var formData = {
            'id':questionId 
        };
        $.ajax({
                type: 'POST',
                url: '/youasked/upvote',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    //var result = JSON.parse(result);
                     console.log(result);
                    if (result.status === "success") {                        
                        $("#up"+questionId).addClass("arrowdim");
                        $("#down"+questionId).removeClass("arrowdim");
                        $("#upvote"+questionId).text(result.votes.suv);
                        $("#downvote"+questionId).text(result.votes.sdv); 
                         if(result.votes.diff > 0){
                          $("#votecolor"+questionId).addClass("upvotetext"); 
                          $("#votecolor"+questionId).removeClass("downvotetext"); 
                        }else{
                          $("#votecolor"+questionId).addClass("downvotetext"); 
                          $("#votecolor"+questionId).removeClass("upvotetext"); 
                        }

                    } else if (result.status === "error") {
                       console.log(result.status);
                    }
                },
                error: function(result) {
                    console.log(result);
                }
            });

    });
    
    
    
    
       $('body').on('click','.votedownbtn',function(event){        
        event.preventDefault();
        var questionId = this.id;
        questionId = questionId.replace("down",'');
        var formData = {
            'id':questionId 
        };
        $.ajax({
                type: 'POST',
                url: '/youasked/downvote',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    //var result = JSON.parse(result);
                    console.log(result);
                    if (result.status === "success") {
                        console.log(result.result);
                        $("#down"+questionId).addClass("arrowdim");
                        $("#up"+questionId).removeClass("arrowdim");
                        $("#upvote"+questionId).text(result.votes.suv);
                        $("#downvote"+questionId).text(result.votes.sdv);
                        if(result.votes.diff > 0){
                          $("#votecolor"+questionId).addClass("upvotetext"); 
                          $("#votecolor"+questionId).removeClass("downvotetext"); 
                        }else{
                          $("#votecolor"+questionId).addClass("downvotetext"); 
                          $("#votecolor"+questionId).removeClass("upvotetext"); 
                        }
                        

                    } else if (result.status === "error") {
                       console.log(result.status);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

    });
    
    
        //show most recent question i.e questions just entered
    $('#mostrecentquestbtn').click(function(event) {
        event.preventDefault();
        $("#questionsContainer").empty();
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="questheading" style="font-size:120%;line-height: 1.3;color:#333">{{this.questtitle}}</div>'+
            '<div class="questexplain"  style="font-size:90%;margin-top:5px; ">{{{this.questtext}}}</div>'+
            '<div class="questuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>'+
            '</div>'+
        '</div>'+

    '</div>'+
'</div>{{/each}}';

        var template = Handlebars.compile(source);
         $.ajax({
                type: 'POST',
                url: '/youasked/mostrecent',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000,'swing');
                    //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "questionsContainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                 }else{
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no New Questions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
      
    });
    
    
    
     //show most recent question i.e questions just entered
    $('#mostvotedquestbtn').click(function(event) {
        event.preventDefault();
        $("#questionsContainer").empty();
        
    var source ='{{#each .}}<div class="row eachsugg" ><div class="col-xs-12" style="min-height:120px;padding:10px;border-bottom: 1px dotted #ddd;">'+
        '<div class="col-xs-1" style="padding-left:30px">'+
            '<button  id="{{this.upbtnid}}" type="button" class="voteupbtn btn btn-default btn-sm {{this.upbtndimclass}}" style="margin-bottom:20px; ">'+
                '<span class="glyphicon glyphicon-chevron-up" ></span>'+
            '</button>'+
            '<br/>'+
            '<button id="{{this.downbtnid}}" type="button" class="votedownbtn btn btn-default btn-sm {{this.downbtndimclass}}" style="margin-top:20px;">'+
                '<span class="glyphicon glyphicon-chevron-down"></span>'+
            '</button>'+
        '</div>'+
        '<div class="col-xs-1" style="margin-left:-20px;height:100px;">'+
            '<div class="upvote" id="{{this.upvoteid}}">{{this.upvotecount}}</div>'+
            '<div class="{{this.votetextclass}}" id="{{this.votecolorid}}">Votes</div>'+
            '<div class="downvote" id="{{this.downvoteid}}">{{this.downvotecount}}</div>'+
        '</div>'+

        '<div class="col-xs-10" style="min-height:80px">'+
           '<div class="questheading" style="font-size:120%;line-height: 1.3;color:#333">{{{this.questtitle}}}</div>'+
            '<div class="questexplain"  style="font-size:90%;margin-top:5px;">{{{this.questtext}}}</div>'+
            '<div class="questuser" style="float:right;margin-top:20px;">'+                              
                '<a href="#" style="font-size:80%;color:#bf6d94;font-weight:bold">{{this.questemail}}</a>'+
            '</div>'+
        '</div>'+
    '</div>'+
'</div>{{/each}}';

      
        var template = Handlebars.compile(source);
         
         $.ajax({
                type: 'POST',
                url: '/youasked/mostvoted',
                data:"",
                dataType: '',
                encode: true,
                cache: false,
                success: function(result) {
                  //console.log(result);
                 if(result.length > 2){
                   var data = JSON.parse(result);
                   var html = template(data);
                    $('#questionsContainer').html(html).fadeIn(4000,'swing');
                     //Navigation for Suggestions Content
                    $("div.holder").jPages({
                        containerID: "questionsContainer",
                       perPage: 10,
                        startPage: 1,
                        startRange: 2,
                        midRange: 5,
                        endRange: 1,
                        first: "first",
                        previous: "previous",
                        next: "next",
                        last: "last",
                        animation :"fadeIn"
                    });
                      
                 }else{
                    $('#questionsContainer').html($('<div class="row"><div class="col-xs-3"></div><div class="col-xs-6"><div class="alert alert-dismissable alert-warning"><h4 style="text-align:center">Oops no Questions!!</h4></div></div><div class="col-xs-2"></div>'));  
                 }
                },                    
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
      
    });
    
 
    
    
    
    
    // make the first tab show after loading data
    $('#pbjTab a[href="#new"]').tab('show') ;

  

//create new questions form validation & form submit
    $('form').submit(function(event) {
        event.preventDefault();
        $("#questsuccess").addClass("hide");
        $("#questerror").addClass("hide");
        var questtitle = $('#questiontitle').val();
        var questtext = $('#questiondesc').val();
        var formData = {
            'questtitle': questtitle,
            'questdesc': questtext
        };
        if (questtitle.length && questtext.length) {
            $.ajax({
                type: 'POST',
                url: '/youasked/createnew',
                data: formData,
                dataType: 'json',
                encode: true,
                cache: false,
                success: function(result) {
                    // var result = JSON.parse(result);
                    console.log(result);
                    if (result.status === "success") {
                        $("#questsuccess").removeClass("hide");                      
                        $("#questsuccess").fadeOut(1200);
                        $('#questiontitle').val('');
                        $('#questiondesc').val('');

                    } else if (result.status === "error") {
                        $("#questerror").removeClass("hide");
                        var errorMsg = result.errormsg.join("<br>");
                        $("#newquesterr").html(errorMsg);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });

        } else {
            $("#questerror").removeClass("hide");
            $("#newquesterr").text('Question Title or Question Text cannot be empty');
        }

    });
    
    
        //Navigation for Suggestions Content
        $("div.holder").jPages({
            containerID: "questionsContainer",
           perPage: 10,
            startPage: 1,
            startRange: 2,
            midRange: 5,
            endRange: 1,
            first: "first",
            previous: "previous",
            next: "next",
            last: "last",
            animation :"fadeIn"
        });
        

});