<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initTimeZone() {
        date_default_timezone_set('America/New_York');
    }

    protected function _initRoutes() {
        $frontcontroller = Zend_Controller_Front::getInstance();
        $router = $frontcontroller->getRouter();

        //route for login
        $loginRoute = new Zend_Controller_Router_Route_Static('login', array('controller' => 'Account', 'action' => 'login'));
        $router->addRoute('login', $loginRoute);


        //route for logout
        $logout = new Zend_Controller_Router_Route_Static('logout', array('controller' => 'Account', 'action' => 'logout'));
        $router->addRoute('logout', $logout);
    }

    protected function _initAutoload() {
        $autoLoader = Zend_Loader_Autoloader::getInstance();
        $resourceLoader = new Zend_Loader_Autoloader_Resource(array(
            'basePath' => APPLICATION_PATH,
            'namespace' => '',
        ));
        $resourceLoader->addResourceType('model', 'models', 'Model');
        $autoLoader->pushAutoloader($resourceLoader);
        return $autoLoader;
    }

    protected function _initDbAdapter() {
        $resource = $this->getPluginResource('db');
        $db = $resource->getDbAdapter();
        Zend_Registry::set("db", $db);
    }
    
    protected function _initLogger(){
        $writer = new Zend_Log_Writer_Stream('C:\Bitnami\wampstack-5.4.28-0\apache2\logs\edsstaplesapp.log');
        $logger = new Zend_Log($writer);
        Zend_Registry::set('Zend_Log', $logger);
        
    }

}
