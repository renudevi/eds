<?php

class FeedbackController extends Zend_Controller_Action
{

    protected $employeeId = null;

    protected $userName = null;

    protected $filterHtmlEntities = null;

    protected $suggestionModel = null;

    protected $employeeSuggVoteModel = null;

    protected $zendlog = null;

    public function init()
    {
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }

        $this->_helper->layout->setLayout('default');
        $this->view->pageTitle = 'EDS Portal | Feedback';
        $esdNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $esdNamespace->employeeName;
        $this->employeeId = $esdNamespace->employeeId;
        $this->view->username = $this->userName;
        $this->view->js = array("handlebars.js", "magnific.js", "feedback.js", "jPages.js");
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
        $this->feedbackModel = new Model_Feedback();
        $this->zendlog = Zend_Registry::get('Zend_Log');
    }

    public function indexAction()
    {
        
    }

    public function submitAction()
    {
        try{
            $feedbacktext = $this->filterHtmlEntities->filter($this->_request->getParam('description'));            
            $this->feedbackModel->createFeedback($this->employeeId, $feedbacktext);           
        } catch (Zend_Exception $ex) {           
            $this->zendlog->log('FeedbackController'.$ex->getMessage());    
        }
       
    }


}



