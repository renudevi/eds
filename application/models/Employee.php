<?php

class Model_Employee extends Zend_Db_Table_Abstract {

    protected $_name = 'employee';
    protected $_primary = 'id';
    protected $dbAdapter;
    
    public function init(){
      $this->dbAdapter =  Zend_Registry::get("db");  
    }
    
    public function addEmployeetoEDS(Array  $employeeDetails) {
        try{
            $row = $this->createRow();
            $row->employee_id = (int) $employeeDetails['employeeid'];
            $row->employee_name = $employeeDetails['name'];
            $row->employee_email = $employeeDetails['email'];
            $row->employee_uid = $employeeDetails['uid'];
            $row->eds_admin = $employeeDetails['isadmin'];            
            return $row->save();                       
        }catch(Zend_Db_Exception $ex){           
          throw new Zend_Db_Exception($ex->getMessage());            
        }        
    }
    
    public function checkEmployeeExists($employeeId){
         try {

            $employeeId = (int) $employeeId;
            $sql = "SELECT employee_id FROM employee WHERE employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($employeeId ));
            $result = $stmt->fetchAll();
            if(count($result)== 1){
                return TRUE;
            }else{
                return FALSE;
            }
        }catch(Zend_Db_Exception $ex){           
          throw new Zend_Db_Exception($ex->getMessage());            
        }  
        
    }
    
    
    public function updateLastLoggedin($employeeId) {
        try {

            $employeeId = (int) $employeeId;           
            $data = array(
                'last_loggedin' => new Zend_Db_Expr('NOW()')
            );
            $where['employee_id = ?'] = $employeeId;
            $n = $this->dbAdapter->update('employee', $data, $where);
        } catch(Zend_Db_Exception $ex){           
          throw new Zend_Db_Exception($ex->getMessage());            
        }  
    }

}
