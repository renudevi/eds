<?php

class Model_Feedback extends Zend_Db_Table_Abstract
{
    
    protected $_name = 'feedback';
    protected $_primary = 'feedback_id';
    protected $dbAdapter;

    public function init() {
        $this->dbAdapter = Zend_Registry::get('db');
    }

    
    public function createFeedback($employeeId, $feedbackText){        
        try {
            $row = $this->createRow();
            $row->feedback_employee_id = $employeeId;
            $row->feedback_text = $feedbackText;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
        
    }


}

