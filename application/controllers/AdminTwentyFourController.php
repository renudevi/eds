<?php

class AdminTwentyFourController extends Zend_Controller_Action {

    protected $employeeId = null;
    protected $userName = null;
    protected $filterHtmlEntities = null;
    protected $twentyfourModel = null;
    protected $employeeTwentyfourVoteModel  = null;
    protected $zendlog = null;

    public function preDispatch() {
        parent::preDispatch();
        try {
            $acccesHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('acl');
            $resource = 'pbjAdminArea';
            if (!$acccesHelper->checkAccessRights($resource)) {
                $this->_helper->redirector('index', 'index');
                return;
            }

            Zend_Layout::getMvcInstance()->assign('heading', 'Twenty Four 2014');
            Zend_Layout::getMvcInstance()->assign('saying', 'Showcase your Ideas, Prototypes & Hacks or vote for your favorite.');
        } catch (Zend_Exception $ex) {
            $this->zendlog->log("AdminTwentyFourController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function init() {
        //check if admin and session exists
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }


        $this->view->pageTitle = 'EDS Portal | 24 Admin';
        $this->view->js = array("handlebars.js", "magnific.js", "admin24.js", "jPages.js");
        $esdNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $esdNamespace->employeeName;
        $this->employeeId = $esdNamespace->employeeId;
        $this->view->username = $this->userName;
        $this->twentyfourModel = new Model_TwentyFour();
        $this->employeeTwentyfourVoteModel = new Model_TwentyFourvote();
        $this->zendlog = Zend_Registry::get('Zend_Log');
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
    }

    /* public function indexAction() {
      try {
      $cache = $this->getInvokeArg('bootstrap')
      ->getResource('cachemanager')
      ->getCache('edscache');
      //var_dump($cache);
      //exit;
      if (!($this->view->newSuggestions = $cache->load('suggestions'))) {
      $viewSuggestions = $this->twentyfourModel->viewDisplayNewSuggestions($this->employeeId);
      $cache->save($viewSuggestions,'suggestions');
      $this->view->newSuggestions = $viewSuggestions;
      }
      } catch (Zend_Exception $ex) {
      $this->zendlog->log("AdminTwentyFourController" . $ex->getMessage(), Zend_Log::INFO);
      }
      } */

    public function indexAction() {
        
    }

    public function ideasAction() {
        try {
            //set default layout
            $this->_helper->layout->setLayout('adminpbj');
            $viewSuggestions = $this->twentyfourModel->viewDisplayNewSuggestions($this->employeeId);
            $this->view->newSuggestions = $viewSuggestions;
        } catch (Zend_Exception $ex) {
            $this->zendlog->log("AdminTwentyFourController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function createnewAction() {
        $errorMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        /* $suggestTitle = $this->filterHtmlEntities->filter($this->getRequest()->getPost('suggtitle', NULL));
          $suggestText = $this->filterHtmlEntities->filter($this->getRequest()->getPost('suggdesc', NULL)); */
        $suggestTitle = $this->getRequest()->getPost('suggtitle', NULL);
        $suggestText = $this->getRequest()->getPost('suggdesc', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 10));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        if (!$noemptyValidator->isValid($suggestTitle)) {
            $errorMsg[] = "Suggestion Title cannot be empty";
        } else if (!$strlengthValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Title length should atleast 10chars ";
        }

        if (!$noemptyValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Title cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($suggestText)) {
            $errorMsg[] = "Suggestion Text length should be atleast 10chars";
        }

        if (!count($errorMsg)) {
            $newSuggestionId = $this->twentyfourModel->createNewSuggestion($this->employeeId, $suggestTitle, $suggestText);
            $this->starterDummySuggestionVote($newSuggestionId);
            if ($newSuggestionId) {
                $result = array(
                    "status" => "success",
                    "newid" => $newSuggestionId
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }

        echo json_encode($result);
    }

    private function getNewSuggestion() {
        try {
            $newSuggestions = $this->twentyfourModel->getNewSuggestion();
            return $newSuggestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage()
            $this->zendlog->log("AdminTwentyFourController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    private function starterDummySuggestionVote($suggestionId) {
       $this->employeeTwentyfourVoteModel->createDummySuggestionVote($suggestionId);
    }

    public function upvoteAction() {
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $suggestionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal');
        $votingEmployeeId = $usersession->employeeId;
        /* $suggestionId = 6;
          $votingEmployeeId = 1; */

        //check badge &  cast vote if user already not voted
        $suggResults = $this->twentyfourModel->getUserSuggestionDetails($suggestionId, $votingEmployeeId);
        // var_dump($suggResults);
        if (count($suggResults[0])) {

            $successUserMsg[] = 'Vote change cast done';
            if ($suggResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($suggResults[0]['uv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($suggResults[0]['dv'] == '1') {
                $updatedVote =$this->employeeTwentyfourVoteModel->reverseCastDownVote($suggestionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId =$this->employeeTwentyfourVoteModel->castUpVote($suggestionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in suggestion table
            $this->twentyfourModel->updateTotalVotesforSuggestionId($suggestionId);
        }

        //send the current up/down votes for the  suggestion
        $suggesVotes =$this->employeeTwentyfourVoteModel->getCurrentVoteStatusBySuggestionId($suggestionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $suggesVotes[0]
            );
        }

        echo json_encode($result);
    }

    public function downvoteAction() {
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $suggestionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;
        /* $suggestionId = 4;
          $votingEmployeeId = 1; */

        //check badge & change up/down votes if user already votes
        $suggResults = $this->twentyfourModel->getUserSuggestionDetails($suggestionId, $votingEmployeeId);
        if (count($suggResults[0])) {
            if ($suggResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($suggResults[0]['dv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($suggResults[0]['uv'] == '1') {
                $updatedVote =$this->employeeTwentyfourVoteModel->reverseCastUpVote($suggestionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId =$this->employeeTwentyfourVoteModel->castDownVote($suggestionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in suggestion table
            $this->twentyfourModel->updateTotalVotesforSuggestionId($suggestionId);
        }

        //send the current up/down votes for the  suggestion
        $suggesVotes =$this->employeeTwentyfourVoteModel->getCurrentVoteStatusBySuggestionId($suggestionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $suggesVotes[0]
            );
        }

        echo json_encode($result);
    }

    public function resolvedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $resolvedSuggestions = $this->twentyfourModel->resolvedSuggestions($votingEmployeeId);
        $resolvedResults = array();
        $results = array();
        foreach ($resolvedSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $resolvedResults[] = $results;
        }
        echo json_encode($resolvedResults);
    }

    public function workinprogressAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $workinprogressSuggestions = $this->twentyfourModel->workinprogressSuggestions($votingEmployeeId);
        $workinprogressResults = array();
        $results = array();
        foreach ($workinprogressSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $workinprogressResults[] = $results;
        }
        echo json_encode($workinprogressResults);
    }

    public function archiveAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $archiveSuggestions = $this->twentyfourModel->archiveSuggestions($votingEmployeeId);
        $archiveResults = array();
        $results = array();
        foreach ($archiveSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $archiveResults[] = $results;
        }
        echo json_encode($archiveResults);
    }

    /**
     * function for submitting the admin comments
     */
    public function admincommentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $suggestionId = $this->getRequest()->getPost('suggestionid', NULL);
        $admincomment = $this->getRequest()->getPost('admincomment', NULL);
        $adminbadge = $this->getRequest()->getPost('adminbadge', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 4));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        $intValidator = new Zend_Validate_Int();

        if (!$intValidator->isValid($suggestionId)) {
            $errorMsg[] = "Invalid Suggestion Id";
        }

        if (!$noemptyValidator->isValid($adminbadge)) {
            $errorMsg[] = "Suggestion Badge cannot be empty";
        } else if (!$strlengthValidator->isValid($adminbadge) || $adminbadge == "none") {
            $errorMsg[] = "Invalid Suggestion Badge";
        }

        if (!$noemptyValidator->isValid($admincomment)) {
            $errorMsg[] = "Admin comment cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($admincomment)) {
            $errorMsg[] = "Admin comment length should be atleast 30chars";
        }

        if (!count($errorMsg)) {
            //$adminId, $admincomment, $adminbadge, $suggestionId
            $createnewAdminSuggest = $this->twentyfourModel->addAdminComments($this->employeeId, $admincomment, $adminbadge, $suggestionId);
            if ($createnewAdminSuggest) {
                $result = array(
                    "status" => "success"
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }

        echo json_encode($result);
    }

    public function adminmostrecentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->adminMostRecentSuggestions($votingEmployeeId);
        /* echo '<pre>';
          print_r($mostrecentSuggestions);
          echo '</pre>';
          return;
          //echo json_encode($mostrecentSuggestions); */
        $newsuggResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {

            foreach ($lookup as $key => $value) {

                if ($key == 'id') {
                    //admin template id
                    $results['suggid'] = 'suggid' . $value;
                    $results['suggidborder'] = 'suggidborder' . $value;

                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;

                    //AdminId values for form

                    $results['edit'] = 'edit' . $value;
                    $results['admincommentid'] = 'admincommentid' . $value;
                    $results['admincommentformid'] = 'admincommentformid' . $value;
                    $results['admincommenttextid'] = 'admincommenttextid' . $value;
                    $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                    $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                    $results['adminformcancelid'] = 'adminformcancelid' . $value;
                    $results['adminsuggsuccess'] = 'adminsuggsuccess' . $value;
                    $results['adminsuggerror'] = 'adminsuggerror' . $value;
                    $results['adminnewsuggerr'] = 'adminnewsuggerr' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newsuggResults[] = $results;
        }
        echo json_encode($newsuggResults);
    }

    public function adminmostvotedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentSuggestions = $this->twentyfourModel->adminMostVotedSuggestions($votingEmployeeId);
        $newsuggResults = array();
        $results = array();
        foreach ($mostrecentSuggestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    //admin template id
                    $results['suggid'] = 'suggid' . $value;
                    $results['suggidborder'] = 'suggidborder' . $value;

                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;

                    //AdminId values for form

                    $results['edit'] = 'edit' . $value;
                    $results['admincommentid'] = 'admincommentid' . $value;
                    $results['admincommentformid'] = 'admincommentformid' . $value;
                    $results['admincommenttextid'] = 'admincommenttextid' . $value;
                    $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                    $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                    $results['adminformcancelid'] = 'adminformcancelid' . $value;
                    $results['adminsuggsuccess'] = 'adminsuggsuccess' . $value;
                    $results['adminsuggerror'] = 'adminsuggerror' . $value;
                    $results['adminnewsuggerr'] = 'adminnewsuggerr' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['suggemail'] = $value;
                }

                if ($key == 'title') {
                    $results['suggtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newsuggResults[] = $results;
        }
        echo json_encode($newsuggResults);
    }

    public function newtabAction() {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $newsuggTabResults = $this->twentyfourModel->viewDisplayNewSuggestions($this->employeeId);
            $newsuggResults = array();
            $results = array();
            foreach ($newsuggTabResults as $lookup) {
                foreach ($lookup as $key => $value) {
                    if ($key == 'id') {
                        //admin template id
                        $results['suggid'] = 'suggid' . $value;
                        $results['suggidborder'] = 'suggidborder' . $value;

                        $results['upbtnid'] = 'up' . $value;
                        $results['downbtnid'] = 'down' . $value;
                        $results['upvoteid'] = 'upvote' . $value;
                        $results['downvoteid'] = 'downvote' . $value;
                        $results['votecolorid'] = 'votecolor' . $value;

                        //AdminId values for form
                        $results['edit'] = 'edit' . $value;
                        $results['admincommentid'] = 'admincommentid' . $value;
                        $results['admincommentformid'] = 'admincommentformid' . $value;
                        $results['admincommenttextid'] = 'admincommenttextid' . $value;
                        $results['adminbadgeselectid'] = 'adminbadgeselectid' . $value;
                        $results['adminformsubmitid'] = 'adminformsubmitid' . $value;
                        $results['adminformcancelid'] = 'adminformcancelid' . $value;
                        $results['adminsuggsuccess'] = 'adminsuggsuccess' . $value;
                        $results['adminsuggerror'] = 'adminsuggerror' . $value;
                        $results['adminnewsuggerr'] = 'adminnewsuggerr' . $value;
                    }

                    if ($key == 'useruv') {
                        if (!is_null($value) && $value != '0') {
                            $results['upbtndimclass'] = 'arrowdim';
                        } else {
                            $results['upbtndimclass'] = '';
                        }
                    }

                    if ($key == 'userdv') {
                        if (!is_null($value) && $value != '0') {
                            $results['downbtndimclass'] = 'arrowdim';
                        } else {
                            $results['downbtndimclass'] = '';
                        }
                    }


                    if ($key == 'suv') {
                        $results['upvotecount'] = $value;
                        $suv = $value;
                    }

                    if ($key == 'sdv') {
                        $results['downvotecount'] = $value;
                        $sdv = $value;
                    }

                    $diff = $suv - $sdv;
                    if ($diff > 0) {
                        $results['votetextclass'] = 'upvotetext';
                    } else {
                        $results['votetextclass'] = 'downvotetext';
                    }

                    if ($key == "email") {
                        $results['suggemail'] = $value;
                    }

                    if ($key == 'title') {
                        $results['suggtitle'] = htmlspecialchars($value);
                    }

                    if ($key == 'text') {
                        $results['suggtext'] = nl2br(htmlspecialchars($value), true);
                    }
                }
                $newsuggResults[] = $results;
            }
            echo json_encode($newsuggResults);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
