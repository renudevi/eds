<?php

class YouAskedController extends Zend_Controller_Action {

    protected $employeeId = null;
    protected $userName = null;
    protected $filterHtmlEntities = null;
    protected $questionModel = null;
    protected $employeeQuestionVoteModel = null;
    protected $zendlog = null;

    public function preDispatch() {
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->assign('heading', 'Ask a Question');
        Zend_Layout::getMvcInstance()->assign('saying', 'Find your answers here');
    }

    public function init() {
        if (!Zend_Session::sessionExists()) {
            $this->_helper->redirector('index', 'index');
        }

        $this->_helper->layout->setLayout('defaultpbj');
        $this->view->pageTitle = 'EDS Portal | Your Questions';
        $esdNamespace = new Zend_Session_Namespace('edsportal');
        $this->userName = $esdNamespace->employeeName;
        $this->employeeId = $esdNamespace->employeeId;
        $this->view->username = $this->userName;
        $this->view->js = array("handlebars.js", "magnific.js", "youasked.js", "jPages.js");
        $this->filterHtmlEntities = new Zend_Filter_HtmlEntities();
        $this->questionModel = new Model_Yourquestion();
        $this->employeeQuestionVoteModel = new Model_Yourquestionemployeevote();
        $this->zendlog = Zend_Registry::get('Zend_Log');
    }

    public function indexAction() {
        try {
            $viewQuestions = $this->questionModel->viewDisplayNewQuestions($this->employeeId);
            $this->view->newQuestions = $viewQuestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage();
            $this->zendlog->log("YourquestionsController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function createnewAction() {
        $errorMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        /* $questionTitle = $this->filterHtmlEntities->filter($this->getRequest()->getPost('questtitle', NULL));
          $questionText = $this->filterHtmlEntities->filter($this->getRequest()->getPost('questdesc', NULL)); */
        $questionTitle = $this->getRequest()->getPost('questtitle', NULL);
        $questionText = $this->getRequest()->getPost('questdesc', NULL);
        $strlengthValidator = new Zend_Validate_StringLength(array('min' => 10));
        $strlengthTextValidator = new Zend_Validate_StringLength(array('min' => 30));
        $noemptyValidator = new Zend_Validate_NotEmpty();
        if (!$noemptyValidator->isValid($questionTitle)) {
            $errorMsg[] = "Question Title cannot be empty";
        } else if (!$strlengthValidator->isValid($questionText)) {
            $errorMsg[] = "Question Title length should atleast 10chars ";
        }

        if (!$noemptyValidator->isValid($questionText)) {
            $errorMsg[] = "Question Title cannot be empty ";
        } else if (!$strlengthTextValidator->isValid($questionText)) {
            $errorMsg[] = "Question Text length should be atleast 10chars";
        }

        if (!count($errorMsg)) {
            $newQuestionId = $this->questionModel->createNewQuestion($this->employeeId, $questionTitle, $questionText);
            $this->starterDummyQuestionVoteAction($newQuestionId);
            if ($newQuestionId) {
                $result = array(
                    "status" => "success",
                    "newid" => $newQuestionId
                );
            }
        } else {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        }

        echo json_encode($result);
    }

    private function getNewQuestionAction() {
        try {
            $newQuestions = $this->questionModel->getNewQuestion();
            return $newQuestions;
        } catch (Zend_Exception $ex) {
            //$ex->getMessage();
            $this->zendlog->log("YourquestionsController" . $ex->getMessage(), Zend_Log::INFO);
        }
    }

    public function starterDummyQuestionVoteAction($questionId) {
        $this->employeeQuestionVoteModel->createDummyQuestionVote($questionId);
    }
    
    
    
    public function upvoteAction() {
        try{
            $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $questionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal');
        $votingEmployeeId = $usersession->employeeId;
        //check badge &  cast vote if user already not voted
        $questResults = $this->questionModel->getUserQuestionDetails($questionId, $votingEmployeeId);
        
        if (count($questResults[0])> 0) {

            $successUserMsg[] = 'Vote change cast done';
            if ($questResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($questResults[0]['uv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($questResults[0]['dv'] == '1') {
                $updatedVote = $this->employeeQuestionVoteModel->reverseCastDownVote($questionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        }else {
            //user did not vote 
            $lastInsertVoteId = $this->employeeQuestionVoteModel->castUpVote($questionId, $votingEmployeeId);           
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in questionion table
            $this->questionModel->updateTotalVotesforQuestionId($questionId);
         }
             
        //send the current up/down votes for the  questionion
        $questesVotes = $this->employeeQuestionVoteModel->getCurrentVoteStatusByQuestionId($questionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $questesVotes[0]
            );
        }

        echo json_encode($result);
            
        }catch(Zend_Exception $ex){
           echo $ex->getMesssage(); 
        }
        
    }


    public function downvoteAction() {
        $errorMsg = array();
        $successUserMsg = array();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        //check session exists
        if (!Zend_Session::sessionExists()) {
            $errorMsg[] = "Please login to vote";
        }
        $questionId = $this->filterHtmlEntities->filter($this->getRequest()->getPost('id', NULL));
        $usersession = new Zend_Session_Namespace('edsportal');
        $votingEmployeeId = $usersession->employeeId;

        //check badge & change up/down votes if user already votes
        $questResults = $this->questionModel->getUserQuestionDetails($questionId, $votingEmployeeId);
        if (count($questResults[0])) {
            if ($questResults[0]['badge'] != "new") {
                $errorMsg[] = 'Sorry voting is closed';
            } else if ($questResults[0]['dv'] == '1') {
                $errorMsg[] = 'Vote already casted';
            } else if ($questResults[0]['uv'] == '1') {
                $updatedVote = $this->employeeQuestionVoteModel->reverseCastUpVote($questionId, $votingEmployeeId);
                if ($updatedVote) {
                    $successUserMsg[] = 'Vote change cast done';
                }
            }
        } else {
            //user did not vote 
            $lastInsertVoteId = $this->employeeQuestionVoteModel->castDownVote($questionId, $votingEmployeeId);
            if ($lastInsertVoteId) {
                $successUserMsg[] = 'Vote cast done';
            }
            //update totalvotes in questionion table
            $this->questionModel->updateTotalVotesforQuestionId($questionId);
        }

        //send the current up/down votes for the  questionion
        $questesVotes = $this->employeeQuestionVoteModel->getCurrentVoteStatusByQuestionId($questionId);

        if (count($errorMsg)) {
            $result = array(
                "status" => "error",
                "errormsg" => $errorMsg
            );
        } else {
            $result = array(
                "status" => "success",
                "successmsg" => $successUserMsg,
                "votes" => $questesVotes[0]
            );
        }

        echo json_encode($result);
    }

    public function mostrecentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->mostrecentQuestions($votingEmployeeId);
        /* echo '<pre>';
          print_r($mostrecentQuestions);
          echo '</pre>';
          return; */
        //echo json_encode($mostrecentQuestions);
        $newquestResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {

            foreach ($lookup as $key => $value) {

                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newquestResults[] = $results;
        }


        echo json_encode($newquestResults);
    }

    public function mostvotedAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->mostVotedQuestions($votingEmployeeId);
        $newquestResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newquestResults[] = $results;
        }
        echo json_encode($newquestResults);
    }

    public function resolvedquestAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->resolvedQuestions($votingEmployeeId);
        $newquestResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $newquestResults[] = $results;
        }
        echo json_encode($newquestResults);
    }

    public function workinprogressAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $usersession = new Zend_Session_Namespace('edsportal', TRUE);
        $votingEmployeeId = $usersession->employeeId;

        $mostrecentQuestions = $this->questionModel->workinprogressQuestions($votingEmployeeId);
        $workinprogressResults = array();
        $results = array();
        foreach ($mostrecentQuestions as $lookup) {
            foreach ($lookup as $key => $value) {
                if ($key == 'id') {
                    $results['upbtnid'] = 'up' . $value;
                    $results['downbtnid'] = 'down' . $value;
                    $results['upvoteid'] = 'upvote' . $value;
                    $results['downvoteid'] = 'downvote' . $value;
                    $results['votecolorid'] = 'votecolor' . $value;
                }

                if ($key == 'useruv') {
                    if (!is_null($value) && $value != '0') {
                        $results['upbtndimclass'] = 'arrowdim';
                    } else {
                        $results['upbtndimclass'] = '';
                    }
                }

                if ($key == 'userdv') {
                    if (!is_null($value) && $value != '0') {
                        $results['downbtndimclass'] = 'arrowdim';
                    } else {
                        $results['downbtndimclass'] = '';
                    }
                }


                if ($key == 'suv') {
                    $results['upvotecount'] = $value;
                    $suv = $value;
                }

                if ($key == 'sdv') {
                    $results['downvotecount'] = $value;
                    $sdv = $value;
                }

                $diff = $suv - $sdv;
                if ($diff > 0) {
                    $results['votetextclass'] = 'upvotetext';
                } else {
                    $results['votetextclass'] = 'downvotetext';
                }

                if ($key == "email") {
                    $results['questemail'] = $value;
                }

                if ($key == 'title') {
                    $results['questtitle'] = htmlspecialchars($value);
                }

                if ($key == 'text') {
                    $results['questtext'] = nl2br(htmlspecialchars($value), true);
                }

                if ($key == 'admincomment') {
                    $results['admincomment'] = nl2br(htmlspecialchars($value), true);
                }
            }
            $workinprogressResults[] = $results;
        }
        echo json_encode($workinprogressResults);
    }

    public function newquesttabAction() {
        try {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $newquestTabResults = $this->questionModel->viewDisplayNewQuestions($this->employeeId);
            $newquestResults = array();
            $results = array();
            $diff = 0;

            foreach ($newquestTabResults as $lookup) {

                foreach ($lookup as $key => $value) {

                    if ($key == 'id') {
                        $results['upbtnid'] = 'up' . $value;
                        $results['downbtnid'] = 'down' . $value;
                        $results['upvoteid'] = 'upvote' . $value;
                        $results['downvoteid'] = 'downvote' . $value;
                        $results['votecolorid'] = 'votecolor' . $value;
                    }

                    if ($key == 'useruv') {
                        if (!is_null($value) && $value != '0') {
                            $results['upbtndimclass'] = 'arrowdim';
                        } else {
                            $results['upbtndimclass'] = '';
                        }
                    }

                    if ($key == 'userdv') {
                        if (!is_null($value) && $value != '0') {
                            $results['downbtndimclass'] = 'arrowdim';
                        } else {
                            $results['downbtndimclass'] = '';
                        }
                    }


                    if ($key == 'suv') {
                        $results['upvotecount'] = $value;
                        $suv = $value;
                    }


                    if ($key == 'sdv') {
                        $results['downvotecount'] = $value;
                        $sdv = $value;
                        $diff = $suv - $sdv;
                    }

                    if ($diff > 0) {
                        $results['votetextclass'] = 'upvotetext';
                    } else {
                        $results['votetextclass'] = 'downvotetext';
                    }

                    if ($key == "email") {
                        $results['questemail'] = $value;
                    }

                    if ($key == 'title') {
                        $results['questtitle'] = htmlspecialchars($value);
                    }

                    if ($key == 'text') {
                        $results['questtext'] = nl2br(htmlspecialchars($value), true);
                    }
                }
                $newquestResults[] = $results;
            }
            echo json_encode($newquestResults);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
