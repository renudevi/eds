<?php

class Model_Pbjsuggestion extends Zend_Db_Table_Abstract {

    protected $_name = 'suggestion';
    protected $_primary = 'suggestion_id';
    protected $dbAdapter;

    public function init() {
        $this->dbAdapter = Zend_Registry::get('db');
    }

    /**
     * Creates new suggestion
     * @param type $userId
     * @param type $suggestionTitle
     * @param type $suggestionText
     * @return type
     */
    public function createNewSuggestion($userId, $suggestionTitle, $suggestionText) {
        try {
            $row = $this->createRow();
            $row->suggestion_employee_id = $userId;
            $row->suggestion_title = $suggestionTitle;
            $row->suggestion_description = $suggestionText;
            return $row->save();
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getNewSuggestion() {
        try {
            $sql = "SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id                   
                    GROUP by suggestion_id
                    HAVING suv > 0 OR sdv > 0
                    ORDER by diff desc,suggestion_id desc";
            $stmt = $this->dbAdapter->query($sql);
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserSuggestionDetails($suggestionId, $votingEmployeeId) {
        try {
            $suggestionId = (int) $suggestionId;
            $votingEmployeeId = (int) $votingEmployeeId;
            $sql = "SELECT s.suggestion_id as id,ev.up_vote as uv, ev.down_vote as dv, s.badge as badge
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = ev.voted_employee_id
                    WHERE s.suggestion_id=? AND ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($suggestionId, $votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function getUserSuggDetailsByEmployeeId($votingEmployeeId) {

        try {
            $votingEmployeeId = (int) $votingEmployeeId;

            $sql = "SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?";
            $stmt = $this->dbAdapter->query($sql, array($votingEmployeeId));
            $result = $stmt->fetchAll();
            return $result;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function viewDisplayNewSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id   
                    WHERE badge = 'new'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function updateTotalVotesforSuggestionId($suggestionId) {
        try {
            $suggestionId = (int) $suggestionId;
            $data = array(
                'total_votes' => new Zend_Db_Expr('total_votes + 1')
            );
            $where['suggestion_id = ?'] = $suggestionId;
            $n = $this->dbAdapter->update('suggestion', $data, $where);
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostrecentSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id 
                    WHERE badge = 'new'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function mostVotedSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    s.badge as badge, s.total_votes as totalvotes, sum(ev.up_vote) - sum(ev.down_vote) as diff
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id 
                    WHERE badge = 'new'
                    GROUP by suggestion_id)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.badge,u.useruv,u.userdv,s.totalvotes,s.diff
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function resolvedSuggestions($votingEmployeeId) {
        try {

            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id   
                    WHERE badge = 'resolved'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function workinprogressSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id   
                    WHERE badge = 'workinprogress'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function archiveSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge,s.admin_comment as admincomment
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id   
                    WHERE badge = 'archive'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function addAdminComments($adminId, $admincomment, $adminbadge, $suggestionId) {
        try {
            $adminId = (int) $adminId;
            $suggestionId = (int) $suggestionId;
            $data = array(
                'admin_comment' => $admincomment,
                'badge' => $adminbadge,
                'admin_id' => $adminId
            );

            $where['suggestion_id = ?'] = $suggestionId;
            $n = $this->dbAdapter->update('suggestion', $data, $where);
            return $n;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostRecentSuggestions($votingEmployeeId) {

        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;

            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,  s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    sum(ev.up_vote) - sum(ev.down_vote) as diff , s.badge as badge, s.admin_comment as admincomment
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id 
                    WHERE badge = 'new'
                    GROUP by suggestion_id
                    ORDER by diff desc,suggestion_id desc)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,s.diff,s.badge,u.useruv,u.userdv,s.admincomment
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid
                ORDER BY s.id desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch(Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

    public function adminMostVotedSuggestions($votingEmployeeId) {
        try {
            //Generate user specific data
            $votingEmployeeId = (int) $votingEmployeeId;
            $userSuggSql = "CREATE TEMPORARY TABLE usercache(SELECT s.suggestion_id as usersuggid,
                    ev.up_vote as useruv, ev.down_vote as userdv
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    WHERE ev.voted_employee_id=?)";
            $userSuggStmt = $this->dbAdapter->query($userSuggSql, array($votingEmployeeId));
            //$result = $userSuggStmt->fetchAll();
            //getting all usersuggestions
            $suggestionsSql = "CREATE TEMPORARY TABLE suggcache(SELECT e.employee_email as email,s.suggestion_title as title, s.suggestion_description as text,sum(ev.up_vote)as suv, sum(ev.down_vote) as sdv, ev.suggestion_id as id,
                    s.badge as badge, s.total_votes as totalvotes,s.admin_comment as admincomment, sum(ev.up_vote) - sum(ev.down_vote) as diff 
                    FROM employee_vote_suggestion ev
                    INNER JOIN suggestion s
                    USING (suggestion_id)
                    INNER JOIN employee e
                    ON e.employee_id = s.suggestion_employee_id 
                    WHERE badge = 'new'
                    GROUP by suggestion_id)";
            $suggestionsStmt = $this->dbAdapter->query($suggestionsSql);
            //$suggResult = $stmt->fetchAll();
            //joining both for view 
            $viewPresentationSql = "select s.email,s.title,s.text,s.suv,s.sdv,s.id,
                s.badge,u.useruv,u.userdv,s.totalvotes,s.admincomment,s.diff
                FROM suggcache as s
                LEFT JOIN usercache as u
                ON s.id = u.usersuggid
                ORDER by diff desc,totalvotes desc";
            $viewPresentationStmt = $this->dbAdapter->query($viewPresentationSql);
            $viewPresentationStmtResult = $viewPresentationStmt->fetchAll();

            $suggestionsDeleteSql = "drop table suggcache";
            $suggestionsDeleteStmt = $this->dbAdapter->query($suggestionsDeleteSql);

            $userSuggDeleteSql = "drop table usercache";
            $userSuggDeleteStmt = $this->dbAdapter->query($userSuggDeleteSql);
            return $viewPresentationStmtResult;
        } catch (Zend_Db_Exception $ex) {
            throw new Zend_Db_Exception($ex->getMessage());
        }
    }

}
