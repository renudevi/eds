<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {

        $this->view->pageTitle = 'EDS Portal';
        if (Zend_Session::sessionExists()) {
            $esdNamespace = new Zend_Session_Namespace('edsportal');
            $this->view->username = $esdNamespace->employeeName;
        }
    }

}
